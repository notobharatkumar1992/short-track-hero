package controller;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Heena on 4/19/2016.
 */
public class SharedDataPreference {


        public static final String PREFS_NAME = "com.noto.shorttrackhero";
        //  public static final String PREFS_KEY = "AOP_PREFS_String";

        public SharedDataPreference() {
            super();
        }

        public void save(Context context, String text,String pref_key) {
            SharedPreferences settings;
            SharedPreferences.Editor editor;

            //settings = PreferenceManager.getDefaultSharedPreferences(context);
            settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE); //1
            editor = settings.edit(); //2

            editor.putString(pref_key, text); //3

            editor.commit(); //4
        }

        public String getValue(Context context,String pref_key) {
            SharedPreferences settings;
            String text;

            //settings = PreferenceManager.getDefaultSharedPreferences(context);
            settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
            text = settings.getString(pref_key, null);
            return text;
        }

        public void clearSharedPreference(Context context) {
            SharedPreferences settings;
            SharedPreferences.Editor editor;

            //settings = PreferenceManager.getDefaultSharedPreferences(context);
            settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
            editor = settings.edit();

            editor.clear();
            editor.commit();
        }

        public void removeValue(Context context,String pref_key) {
            SharedPreferences settings;
            SharedPreferences.Editor editor;

            settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
            editor = settings.edit();

            editor.remove(pref_key);
            editor.commit();
            //}
        }
    }


