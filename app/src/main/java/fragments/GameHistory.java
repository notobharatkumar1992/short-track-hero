package fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.HomeScreen;
import com.noto.shorttrackhero.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import adapters.FanListAdapter;
import adapters.GamehistoryAdapter;
import adapters.ShophistoryAdapter;
import adapters.SponcerBannerAdapter;
import interfaces.OnReciveServerResponse;
import model.Driver;
import model.DriverDetailModel;
import model.FanModel;
import model.GameHistoryModel;
import model.Get_News_Model;
import model.LoginModel;
import model.PostAysnc_Model;
import model.ProductListModel;
import model.RaceDetailModel;
import model.SponcerModel;

/**
 * Created by admin on 30-06-2016.
 */
public class GameHistory extends Fragment implements OnReciveServerResponse {
    private View rootview;

    private Bundle bundle;
    private RaceDetailModel racedetailModel;

    private Driver driver;

    private RecyclerView fanlist;
    ImageView driver_img;
    TextView driver_name, fans, total_races, total_wins, rank;
    private FanModel fanModel;
    private int status;
    LoginModel getProfile_model;
    private int user_id;
    private Get_News_Model newsModel;
    private TextView news;
    private SponcerModel sponcerModel;
    DriverDetailModel driverDetailModel;
    private RecyclerView sponcer;
    private GameHistoryModel productmodel;
    private String identifier;
    private RecyclerView game_history;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.game_history, container, false);
        identifier = AppDelegate.getValue(getActivity(), Tags.IDENTIFIER);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeObjects();
    }

    private void parseProfile() {
        String Register = AppDelegate.getValue(getActivity(), Parameters.saveLogin);
        AppDelegate.Log("response", Register + "");
        try {
            JSONObject jsonObject = new JSONObject(Register);
            status = jsonObject.getInt("status");
            getProfile_model = new LoginModel();
            if (status == 1) {
                JSONObject response = jsonObject.getJSONObject("response");
                getProfile_model.setId(response.getInt("id"));
                user_id = getProfile_model.getId();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void get_PointsHistory() {
        parseProfile();
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, user_id, ServerRequestConstants.Key_PostintValue);
                PostAsync mPostAsyncObj;
                if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                    mPostAsyncObj = new PostAsync(getActivity(), GameHistory.this, ServerRequestConstants.DRIVER_GAME_HISTORY,
                            mPostArrayList, GameHistory.this);
                    AppDelegate.showProgressDialog(getActivity());
                    mPostAsyncObj.execute();
                } else {

                }

            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    private void initializeObjects() {
        TextView txttitle = (TextView) HomeScreen.toolbar.findViewById(R.id.title);
        txttitle.setText("Game History");
        TextView save = (TextView) HomeScreen.toolbar.findViewById(R.id.save);
        save.setVisibility(View.GONE);
        ImageView search = (ImageView) HomeScreen.toolbar.findViewById(R.id.search);
        search.setVisibility(View.GONE);
        game_history = (RecyclerView) rootview.findViewById(R.id.game_history);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        get_PointsHistory();

    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(getActivity());
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(getActivity(), " Please Try Again!", "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.DRIVER_GAME_HISTORY)) {
            AppDelegate.save(getActivity(), result, Parameters.gameHistory);
            parseFan_shopHistory();
        }
    }


    private void parseFan_shopHistory() {
        String driver_response = AppDelegate.getValue(getActivity(), Parameters.gameHistory);
        try {
            JSONObject jsonObject = new JSONObject(driver_response);
            productmodel = new GameHistoryModel();

            productmodel.setStatus(jsonObject.getInt("status"));

            if (productmodel.getStatus() == 1) {
                JSONObject object = jsonObject.getJSONObject("response");
                JSONArray jsonArray = object.getJSONArray("point_history");
                ArrayList<GameHistoryModel> shopHistoryArray = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject obj = jsonArray.getJSONObject(i);
                    productmodel = new GameHistoryModel();
                    productmodel.setId(obj.getInt("id"));
                    productmodel.setFan_name(obj.getString("fan_name"));
                    productmodel.setDriver_name(obj.getString("driver_name"));
                    productmodel.setCompetitions_name(obj.getString("competitions_name"));
                    productmodel.setRace_name(obj.getString("race_name"));
                    productmodel.setDate(obj.getString("date"));
                    productmodel.setTime(obj.getString("time"));
                    productmodel.setAddress(obj.getString("address"));
                    productmodel.setCurrent_rank(obj.getString("current_rank"));
                    productmodel.setPosition(obj.getInt("position"));
                    productmodel.setTotal_credit(obj.getInt("total_credit"));
                    productmodel.setImage(obj.getString("image"));
                    productmodel.setTotal_point(obj.getInt("total_point"));
                    productmodel.setLap(obj.getInt("lap"));
                    shopHistoryArray.add(productmodel);
                }
                setHistory(shopHistoryArray);
            } else if (productmodel.getStatus() == 0) {
                AppDelegate.ShowDialog(getActivity(), jsonObject.getString("message"), "Alert !!!");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void setHistory(ArrayList<GameHistoryModel> pointsHistoryArray) {
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        GamehistoryAdapter adapter = new GamehistoryAdapter(getActivity(), pointsHistoryArray);
        game_history.setLayoutManager(layoutManager);
        game_history.setAdapter(adapter);
    }

    private void setdriverSponcer(ArrayList<SponcerModel> sponcerModelArrayList) {

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        SponcerBannerAdapter adapter = new SponcerBannerAdapter(getActivity(), sponcerModelArrayList);
        sponcer.setLayoutManager(layoutManager);
        sponcer.setAdapter(adapter);
    }

    private void parse_news() {
        String Citylist = AppDelegate.getValue(getActivity(), Parameters.get_news);
        try {
            JSONObject jsonObject = new JSONObject(Citylist);
            newsModel = new Get_News_Model();
            newsModel.setStatus(jsonObject.getInt("status"));
            newsModel.setDataflow(jsonObject.getInt("dataFlow"));
            if (newsModel.getStatus() == 1) {
                JSONObject jsonArray = jsonObject.getJSONObject("response");
                newsModel.setDescription(jsonArray.getString("Description"));
                news.setText(newsModel.getDescription() + "");
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void parseFan_List() {
        String Fan_list = AppDelegate.getValue(getActivity(), Parameters.fan_list);
        try {
            ArrayList<FanModel> fanList = new ArrayList<>();
            JSONObject object = new JSONObject(Fan_list);
            fanModel = new FanModel();
            fanModel.setStatus(object.getInt("status"));
            fanModel.setDataFlow(object.getInt("dataFlow"));
            if (fanModel.getStatus() == 1) {
                JSONObject response = object.getJSONObject("response");
                JSONArray FAN = response.getJSONArray("Fan List");
                for (int i = 0; i < FAN.length(); i++) {
                    JSONObject FanDetail = FAN.getJSONObject(i);
                    fanModel = new FanModel();
                    fanModel.setFan_id(FanDetail.getInt("Fan_id"));
                    fanModel.setFan_name(FanDetail.getString("name"));
                    fanModel.setFan_image(FanDetail.getString("image"));
                    fanModel.setCreated(FanDetail.getString("created"));
                    fanList.add(fanModel);
                }
                setFanLIST(fanList);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setFanLIST(ArrayList<FanModel> fanList) {
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        FanListAdapter adapter = new FanListAdapter(getActivity(), fanList);
        fanlist.setLayoutManager(layoutManager);
        fanlist.setAdapter(adapter);
    }

    private void showSponcer(final ArrayList<SponcerModel> sponcerModels) {
        /*LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        SponcerBannerAdapter adapter = new SponcerBannerAdapter(getActivity(), sponcerModels);
        banners.setLayoutManager(layoutManager);
        banners.setAdapter(adapter);*/
    }
}
