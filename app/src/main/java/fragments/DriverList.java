package fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.HomeScreen;
import com.noto.shorttrackhero.R;

import java.util.ArrayList;

import Constants.Tags;
import adapters.DriverListAdapter;
import adapters.SponcerBannerAdapter;
import model.Driver;
import model.Race;
import model.RaceDetailModel;

/**
 * Created by admin on 30-06-2016.
 */
public class DriverList extends Fragment {
    private View rootview;

    private Bundle bundle;
    private RaceDetailModel racedetailModel=new RaceDetailModel();

    private ArrayList<Driver> driverModel=new ArrayList<>();
    private RecyclerView driverlist;
    private int key=0;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.driver, container, false);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bundle = getArguments();
         key=bundle.getInt("key",0);
        if(key==2) {
            racedetailModel = bundle.getParcelable(Tags.parcel_Detail);
        }
        initializeObjects();
    }

    private void initializeObjects() {
        TextView txttitle = (TextView) HomeScreen.toolbar.findViewById(R.id.title);
        txttitle.setText("Short Track Hero");
        TextView save = (TextView) HomeScreen.toolbar.findViewById(R.id.save);
        save.setVisibility(View.GONE);
        ImageView search = (ImageView) HomeScreen.toolbar.findViewById(R.id.search);
        search.setVisibility(View.VISIBLE);
        driverlist = (RecyclerView) rootview.findViewById(R.id.driverslist);
        if(racedetailModel!=null && key==2) {
            driverModel = racedetailModel.getDriver();
            AppDelegate.Log("RACEDETAILMODEL,", racedetailModel + "");
            AppDelegate.Log("RACE", driverModel + "");
            if (driverModel.size() > 0) {
                LinearLayoutManager layoutManager
                        = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                DriverListAdapter adapter = new DriverListAdapter(getActivity(), driverModel);
                driverlist.setLayoutManager(layoutManager);
                driverlist.setAdapter(adapter);
            } else {
                AppDelegate.ShowDialog(getActivity(), "No driver Available.", "Alert!!!");
            }
        }else{
            driverlist.setAdapter(null);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }
}
