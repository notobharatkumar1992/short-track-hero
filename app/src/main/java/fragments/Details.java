package fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.HomeScreen;
import com.noto.shorttrackhero.R;

import java.util.ArrayList;

import Constants.Tags;
import adapters.DetailsAdapter;
import model.Race;
import model.RaceDetailModel;
import model.RankList;

/**
 * Created by admin on 30-06-2016.
 */
public class Details extends Fragment {
    private View rootview;
    TextView first_winner,second_winner,first_runner_up,second_runner_up,others;
    private Bundle bundle;
    private RaceDetailModel racedetailModel;
    private Race raceModel;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.details, container, false);
        return rootview;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bundle = getArguments();
        racedetailModel = bundle.getParcelable(Tags.parcel_Detail);
        initializeObjects();
    }
    private void initializeObjects() {
        TextView txttitle = (TextView) HomeScreen.toolbar.findViewById(R.id.title);
        txttitle.setText("Short Track Hero");
        TextView  save = (TextView) HomeScreen.toolbar.findViewById(R.id.save);
        save.setVisibility(View.GONE);
        ImageView search = (ImageView) HomeScreen.toolbar.findViewById(R.id.search);
        search.setVisibility(View.VISIBLE);
        raceModel=racedetailModel.getRace();
        ArrayList<RankList> rankList=raceModel.getRanklist();
        ListView details=(ListView)rootview.findViewById(R.id.finishing_order);
        details.setClickable(false);
        DetailsAdapter adapter=new DetailsAdapter(getActivity(),rankList);
        AppDelegate.LogT(rankList+"");
        details.setAdapter(adapter);
       /* first_winner=(TextView)rootview.findViewById(R.id.first_winner);
        second_winner=(TextView)rootview.findViewById(R.id.second_winner);
        first_runner_up=(TextView)rootview.findViewById(R.id.first_runnerup);
        second_runner_up=(TextView)rootview.findViewById(R.id.second_runnerup);
        others=(TextView)rootview.findViewById(R.id.other);
        raceModel=racedetailModel.getRace();
        AppDelegate.Log("RACEDETAILMODEL,",racedetailModel+"");
        AppDelegate.Log("RACE",raceModel+"");
        first_winner.setText(raceModel.getFirst_winner()+"");
        second_winner.setText(raceModel.getSecond_winner()+"");
        first_runner_up.setText(raceModel.getFirst_runner_up()+"");
        second_runner_up.setText(raceModel.getSecond_runner_up()+"");
        others.setText(raceModel.getOther()+"");*/
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
