package fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.HomeScreen;
import com.noto.shorttrackhero.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import ExpendibleRecyclerView.ChildGetterSetter;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import model.Driver;
import model.Get_News_Model;
import model.LoginModel;
import model.PostAysnc_Model;
import model.Race;
import model.RaceDetailModel;
import model.RaceHistroyModel;
import model.RankList;
import model.SponcerModel;
import utils.DateUtils;

public class RaceHistoryDetail extends Fragment implements OnReciveServerResponse, View.OnClickListener, OnDialogClickListener {
    private Bundle bundle = new Bundle();
    private ChildGetterSetter raceModel;
    public static View rootview;
    FrameLayout frame;
    RecyclerView banners;
    TextView news, competition_title, class_race_title, date, time, laps, address, endday, endtime, racedetail, drivers;
    private String title;
    private RaceDetailModel raceDetailModel;
    private Race racemodel;
    private SponcerModel sponcerModel;
    private Driver driverModel;
    private Get_News_Model newsModel;
    private int status;
    LoginModel getProfile_model;
    private int user_id;
    private TextView interested;
    private String identifier;
    private ImageView search;
    private int key;
    String[] startdatetime;
    private boolean result;
    private String[] enddatetime;
    public static TextView fan_points;
    private RaceHistroyModel racehistoryModel;
    ArrayList<Driver> mydriver;
    private Bundle bundle1;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.race_history_detail, container, false);
        identifier = AppDelegate.getValue(getActivity(), Tags.IDENTIFIER);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bundle = getArguments();
        racehistoryModel = bundle.getParcelable(Tags.parcel_racehistoryDetail);
        initializeObjects();
       // get_news();
    }

    private void parseProfile() {
        String Register = AppDelegate.getValue(getActivity(), Parameters.saveLogin);
        AppDelegate.Log("response", Register + "");
        try {
            JSONObject jsonObject = new JSONObject(Register);
            status = jsonObject.getInt("status");
            getProfile_model = new LoginModel();
            if (status == 1) {
                JSONObject response = jsonObject.getJSONObject("response");
                getProfile_model.setId(response.getInt("id"));
                user_id = getProfile_model.getId();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void get_news() {
        parseProfile();
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, user_id, ServerRequestConstants.Key_PostintValue);
                PostAsync mPostAsyncObj;
                if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                    mPostAsyncObj = new PostAsync(getActivity(), RaceHistoryDetail.this, ServerRequestConstants.DRIVER_GET_NEWS,
                            mPostArrayList, null);
                } else {
                    mPostAsyncObj = new PostAsync(getActivity(), RaceHistoryDetail.this, ServerRequestConstants.GET_NEWS,
                            mPostArrayList, null);
                }
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    private void initializeObjects() {
        TextView txttitle = (TextView) HomeScreen.toolbar.findViewById(R.id.title);
        txttitle.setText("Race History Detail");
        TextView save = (TextView) HomeScreen.toolbar.findViewById(R.id.save);
        save.setVisibility(View.GONE);
        search = (ImageView) HomeScreen.toolbar.findViewById(R.id.search);
        search.setVisibility(View.GONE);

        news = (TextView) rootview.findViewById(R.id.news);
        competition_title = (TextView) rootview.findViewById(R.id.title);
        class_race_title = (TextView) rootview.findViewById(R.id.class_race_title);
        date = (TextView) rootview.findViewById(R.id.day);
        time = (TextView) rootview.findViewById(R.id.time);
        laps = (TextView) rootview.findViewById(R.id.laps);
        address = (TextView) rootview.findViewById(R.id.address);
        frame = (FrameLayout) rootview.findViewById(R.id.frame_race);
         /*banners = (RecyclerView) rootview.findViewById(R.id.banner);*/
        endday = (TextView) rootview.findViewById(R.id.endday);
        endtime = (TextView) rootview.findViewById(R.id.endtime);
        racedetail = (TextView) rootview.findViewById(R.id.racedetails);
        drivers = (TextView) rootview.findViewById(R.id.drivers);
        /*drivers.setOnClickListener(this);*/

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        execute_raceDetail();
    }

    private void execute_raceDetail() {
        parseProfile();
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.race_id, racehistoryModel.getRace_id(), ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.comp_id, racehistoryModel.getCompetition_id(), ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, user_id, ServerRequestConstants.Key_PostintValue);
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(getActivity(), this, ServerRequestConstants.RACE_HISTORY_DETAIL,
                        mPostArrayList, RaceHistoryDetail.this);
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(getActivity());
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(getActivity(), "Service Time out!!!", "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.RACE_HISTORY_DETAIL)) {

            parse_raceDetails(result);
        } else if (apiName.equals(ServerRequestConstants.GET_NEWS)) {
            AppDelegate.save(getActivity(), result, Parameters.get_news);
            AppDelegate.Log("NEWS", result);
            parse_news();
        } else if (apiName.equals(ServerRequestConstants.DRIVER_GET_NEWS)) {
            AppDelegate.save(getActivity(), result, Parameters.get_news);
            AppDelegate.Log("NEWS", result);
            parse_news();
        }
    }


    private void parse_news() {
        String Citylist = AppDelegate.getValue(getActivity(), Parameters.get_news);
        try {
            JSONObject jsonObject = new JSONObject(Citylist);
            newsModel = new Get_News_Model();
            newsModel.setStatus(jsonObject.getInt("status"));
            newsModel.setDataflow(jsonObject.getInt("dataFlow"));
            if (newsModel.getStatus() == 1) {
                JSONObject jsonArray = jsonObject.getJSONObject("response");
                newsModel.setDescription(jsonArray.getString("Description"));
                SpannableStringBuilder ssb = new SpannableStringBuilder("");
                String[] newsArray = (newsModel.getDescription() + "").split("\\%", -1);
                int counter = 0;
                Log.d("test", "newsArray" + newsArray[0] + "news => " + newsArray.length);
                for (int i = 0; i <= newsArray.length - 1; i++) {
                    if (newsArray[i].length() > 0) {
                        Log.d("test", "text => " + newsArray[i] + " , " + newsArray[i].length() + ", i length = " + i + ", counter = " + counter);
                        ssb.append(newsArray[i] + " ");
                        counter += newsArray[i].length() + 1;
                        Bitmap smiley = BitmapFactory.decodeResource(getResources(), R.drawable.flag);
                        try {
                            if (counter > 0)
                                ssb.setSpan(new ImageSpan(smiley, ImageSpan.ALIGN_BASELINE), counter - 1, counter, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                news.setSelected(true);
                news.setText(ssb);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void parse_raceDetails(String result) {
        try {
            JSONObject obj = new JSONObject(result);
            raceDetailModel = new RaceDetailModel();
            raceDetailModel.setStatus(obj.getInt("status"));
            raceDetailModel.setDataflow(obj.getInt("dataFlow"));
            if (raceDetailModel.getStatus() == 1) {
                JSONObject response = obj.getJSONObject("response");
               /* get race response*/
                JSONObject raceobject = response.getJSONObject("Race");
                racemodel = new Race();
                racemodel.setRace_id(raceobject.getInt("id"));
                racemodel.setName(raceobject.getString("name"));
                racemodel.setStart_date(raceobject.getString("start_date"));
                racemodel.setEnd_date(raceobject.getString("end_date"));
                raceDetailModel.setRace(racemodel);
                showDetails();
                /*  get driver response*/
                try {
                    ArrayList<Driver> driver = new ArrayList<>();
                    JSONArray driverobject = response.getJSONArray("Alldriver");
                    driverModel = new Driver();
                    for (int i = 0; i < driverobject.length(); i++) {
                        JSONObject driver_obj = driverobject.getJSONObject(i);
                        driverModel = new Driver();
                        driverModel.setDriver_id(driver_obj.getInt("id"));
                        driverModel.setUsername(driver_obj.getString("username"));
                        driverModel.setDriver_address(driver_obj.getString("address"));
                        driverModel.setTotal_win(driver_obj.getString("total_win"));
                        driverModel.setTotal_race(driver_obj.getInt("total_race"));
                        driverModel.setCurrent_rank(driver_obj.getInt("current_rank"));
                        driverModel.setTotal_credit(driver_obj.getInt("total_credit"));
                        driverModel.setTotal_point(driver_obj.getInt("total_point"));
                        driverModel.setDriver_img(driver_obj.getString("img"));
                        driver.add(driverModel);
                    }
                    raceDetailModel.setDriver(driver);
                    bundle1=new Bundle();
                    bundle1.putParcelableArrayList(Tags.DriverList,driver );
                    racedetail.setOnClickListener(this);
                    AppDelegate.showFragment(getChildFragmentManager(), new RaceDriverList(), R.id.frame_race, bundle1, null);
                    AppDelegate.LogT("driver size." + driver.size() + "add");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    mydriver = new ArrayList<>();
                    JSONArray driverobj = response.getJSONArray("myDriver");
                    driverModel = new Driver();
                    for (int i = 0; i < driverobj.length(); i++) {
                        JSONObject driver_obj = driverobj.getJSONObject(i);
                        driverModel = new Driver();
                        driverModel.setDriver_id(driver_obj.getInt("id"));
                        driverModel.setUsername(driver_obj.getString("username"));
                        driverModel.setDriver_address(driver_obj.getString("address"));
                        driverModel.setTotal_win(driver_obj.getString("total_win"));
                        driverModel.setTotal_race(driver_obj.getInt("total_race"));
                        driverModel.setCurrent_rank(driver_obj.getInt("current_rank"));
                        driverModel.setTotal_credit(driver_obj.getInt("total_credit"));
                        driverModel.setTotal_point(driver_obj.getInt("total_point"));
                        driverModel.setPosition(driver_obj.getInt("position"));
                        driverModel.setPurchase_point(driver_obj.getString("purchase_point"));
                        driverModel.setFan_id(driver_obj.getInt("fan_id"));
                        driverModel.setDriver_img(driver_obj.getString("img"));
                        mydriver.add(driverModel);
                    }
                    drivers.setOnClickListener(this);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                drivers.setOnClickListener(this);
                bundle=new Bundle();
                bundle.putParcelableArrayList(Tags.MyDriverList, mydriver);
                AppDelegate.Log("raceDetailModel", raceDetailModel + "");
            } else {
                AppDelegate.ShowDialog(getActivity(), "No record found.", "Alert!!!");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void showDetails() {
        competition_title.setText(racehistoryModel.getCompetition_name() + "");
        class_race_title.setText(racemodel.getName() + "");
        startdatetime = racemodel.getStart_date().split(" ");
        String sdate = null,stime=null;
        try {
            sdate = new SimpleDateFormat("dd MMM yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(startdatetime[0]));
            stime = new SimpleDateFormat("hh:mm:ss a").format(new SimpleDateFormat("HH:mm:ss").parse(startdatetime[1]));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        date.setText(sdate+ "");
        time.setText(stime + "");
        enddatetime = racemodel.getEnd_date().split(" ");
        endday.setText(enddatetime[0] + "");
        endtime.setText(enddatetime[1] + "");
        // address.setText(racemodel.getAddresss() + "");
    }

    @Override
    public void onClick(View v) {
        switch ((v.getId())) {
            case R.id.racedetails:
                racedetail.setBackgroundResource(R.drawable.yellowbtn);
                racedetail.setTextColor(getResources().getColor(R.color.black));
                drivers.setBackgroundColor(getResources().getColor(R.color.black));
                drivers.setTextColor(getResources().getColor(android.R.color.white));
                AppDelegate.showFragment(getChildFragmentManager(), new RaceDriverList(), R.id.frame_race, bundle1, null);
                break;
            case R.id.drivers:
                drivers.setBackgroundResource(R.drawable.yellowbtn);
                drivers.setTextColor(getResources().getColor(R.color.black));
                racedetail.setBackgroundColor(getResources().getColor(R.color.black));
                racedetail.setTextColor(getResources().getColor(android.R.color.white));
                AppDelegate.showFragment(getChildFragmentManager(), new MyDriverList(), R.id.frame_race, bundle, null);
                break;

        }
    }


    @Override
    public void setOnDialogClickListener(String name) {

    }
}
