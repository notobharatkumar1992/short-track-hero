package fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.HomeScreen;
import com.noto.shorttrackhero.R;

import java.util.ArrayList;

import Constants.Tags;
import adapters.DetailsAdapter;
import model.Race;
import model.RaceDetailModel;
import model.RankList;
import model.SponsorDetail;

/**
 * Created by admin on 30-06-2016.
 */
public class SponsorDetails extends Fragment {
    private View rootview;
    TextView company_name,phone,address,product;
    private Bundle bundle;
    private SponsorDetail sponsor;
    private Race raceModel;
ArrayList<SponsorDetail> sponsorDetails;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.sponsor_details, container, false);
        return rootview;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bundle = getArguments();
        sponsor = bundle.getParcelable(Tags.sponsor_detail);
        initializeObjects();
    }
    private void initializeObjects() {
        TextView txttitle = (TextView) HomeScreen.toolbar.findViewById(R.id.title);
        txttitle.setText("Sponsor Details");
        TextView  save = (TextView) HomeScreen.toolbar.findViewById(R.id.save);
        save.setVisibility(View.GONE);
        ImageView search = (ImageView) HomeScreen.toolbar.findViewById(R.id.search);
        search.setVisibility(View.GONE);
        company_name=(TextView)rootview.findViewById(R.id.company_name);
        phone=(TextView)rootview.findViewById(R.id.phone);
        address=(TextView)rootview.findViewById(R.id.address);
        product=(TextView)rootview.findViewById(R.id.product);
        company_name.setText(sponsor.getCompany_name()+"");
        phone.setText(sponsor.getPhone()+"");
        address.setText(sponsor.getAddress()+"");
        final ImageView image=(ImageView)rootview.findViewById(R.id.image);
        AppDelegate.getInstance(getActivity()).getImageLoader().get(sponsor.getImage(), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppDelegate.LogE(error);
            }
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null)
                    image.setImageBitmap(response.getBitmap());
            }
        });
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
