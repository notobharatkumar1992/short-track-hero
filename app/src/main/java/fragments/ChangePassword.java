package fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.HomeScreen;
import com.noto.shorttrackhero.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import adapters.CityAdapter;
import adapters.CountryAdapter;
import adapters.StateAdapter;
import adapters.TrackAdapter;
import interfaces.OnReciveServerResponse;
import model.CityListModel;
import model.CountryListModel;
import model.GetProfile_Model;
import model.PostAysnc_Model;
import model.StateListModel;
import model.TrackListModel;

import static android.media.ExifInterface.ORIENTATION_NORMAL;
import static android.media.ExifInterface.TAG_ORIENTATION;


/**
 * Created by admin on 16-06-2016.
 */
public class ChangePassword extends Fragment implements OnReciveServerResponse, View.OnClickListener {
    View rootview;
    EditText confirm_password;
    TextView submit;
    private String device_id;
    private int status;
    GetProfile_Model getProfile_model;
    private TextView save;
    private String identifier;
    private String message;
    private EditText confirm;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.change_password, container, false);
        identifier = AppDelegate.getValue(getActivity(), Tags.IDENTIFIER);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeObjects();
        submit.setOnClickListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void initializeObjects() {
        confirm_password = (EditText) rootview.findViewById(R.id.new_pass);
        confirm = (EditText) rootview.findViewById(R.id.confirm_password);
        submit = (TextView) rootview.findViewById(R.id.submit);
        save = (TextView) HomeScreen.toolbar.findViewById(R.id.save);
        save.setVisibility(View.GONE);
        TextView txttitle = (TextView) HomeScreen.toolbar.findViewById(R.id.title);
        txttitle.setText("Change Password");
        ImageView search = (ImageView) HomeScreen.toolbar.findViewById(R.id.search);
        search.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch ((v.getId())) {

            case R.id.submit:
                boolean result = validateall(confirm_password.getText().toString(), confirm.getText().toString());
                if (result == true) {
                    execute_changePassword();
                }
                break;
        }
    }

    private void execute_changePassword() {
        parseProfile();
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.password, confirm_password.getText().toString() + "");
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, getProfile_model.getId(), ServerRequestConstants.Key_PostintValue);
                PostAsync mPostAsyncObj;
                if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                    mPostAsyncObj = new PostAsync(getActivity(), ChangePassword.this, ServerRequestConstants.DRIVER_CHANGE_PASSWORD,
                            mPostArrayList, ChangePassword.this);
                } else {
                    mPostAsyncObj = new PostAsync(getActivity(), ChangePassword.this, ServerRequestConstants.CHANGE_PASSWORD,
                            mPostArrayList, ChangePassword.this);
                }
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    private boolean validateall(String password, String confirm) {
        boolean validate = true;
        try {

            if (password.isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Please enter password", "Alert!!!");
            } else if (confirm.isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Please enter confirm password", "Alert!!!");
            } else if (!password.equals(confirm)) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Confirm password does not match.", "Alert!!!");
            } else {
                validate = AppDelegate.password_validation(getActivity(), password);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        return validate;
    }

    private void parseProfile() {
        String Register = AppDelegate.getValue(getActivity(), Parameters.get_profile);
        AppDelegate.Log("response", Register + "");
        try {
            JSONObject jsonObject = new JSONObject(Register);
            status = jsonObject.getInt("status");
            getProfile_model = new GetProfile_Model();
            if (status == 1) {
                JSONObject response = jsonObject.getJSONObject("response");
                getProfile_model.setId(response.getInt("id"));
            }
            if (status == 0) {
                AppDelegate.ShowDialog(getActivity(), "Please Try again!!!", "Alert!!!");
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(getActivity());
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(getActivity(), "Service Time out!!!", "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.CHANGE_PASSWORD)) {
            AppDelegate.save(getActivity(), result, Parameters.Change_Password);
            parseChange_Pass();
        }
    }

    private void parseChange_Pass() {
        String Citylist = AppDelegate.getValue(getActivity(), Parameters.Change_Password);
        AppDelegate.Log("change pass response", Citylist + "");
        try {
            JSONObject jsonObject = new JSONObject(Citylist);
            int status = jsonObject.getInt("status");
            if (status == 1) {
                AppDelegate.ShowDialog(getActivity(), "Password Change Successfully.", "Alert!!!");
                confirm.setText("");
                confirm_password.setText("");
            } else {
                AppDelegate.ShowDialog(getActivity(), "Bad Response", "Alert!!!");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
