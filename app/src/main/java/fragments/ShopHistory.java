package fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.HomeScreen;
import com.noto.shorttrackhero.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import adapters.FanListAdapter;
import adapters.PointshistoryAdapter;
import adapters.ShophistoryAdapter;
import adapters.SponcerBannerAdapter;
import interfaces.OnReciveServerResponse;
import model.Driver;
import model.DriverDetailModel;
import model.FanModel;
import model.Get_News_Model;
import model.LoginModel;
import model.PointsHistoryModel;
import model.PostAysnc_Model;
import model.ProductListModel;
import model.RaceDetailModel;
import model.SponcerModel;

/**
 * Created by admin on 30-06-2016.
 */
public class ShopHistory extends Fragment implements OnReciveServerResponse {
    private View rootview;

    private Bundle bundle;
    private RaceDetailModel racedetailModel;

    private Driver driver;

    private RecyclerView fanlist;
    ImageView driver_img;
    TextView driver_name,fans,total_races,total_wins,rank;
    private FanModel fanModel;
    private int status;
    LoginModel getProfile_model;
    private int user_id;
    private Get_News_Model newsModel;
    private TextView news;
    private SponcerModel sponcerModel;
    DriverDetailModel driverDetailModel;
    private RecyclerView sponcer;


    private RecyclerView shop_history;
    private ProductListModel productmodel;
    private String identifier;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.point_history, container, false);
        identifier = AppDelegate.getValue(getActivity(), Tags.IDENTIFIER);
        return rootview;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeObjects();
    }
    private void parseProfile() {
        String Register = AppDelegate.getValue(getActivity(), Parameters.saveLogin);
        AppDelegate.Log("response", Register + "");
        try {
            JSONObject jsonObject = new JSONObject(Register);
            status = jsonObject.getInt("status");
            getProfile_model = new LoginModel();
            if (status == 1) {
                JSONObject response = jsonObject.getJSONObject("response");
                getProfile_model.setId(response.getInt("id"));
                user_id = getProfile_model.getId();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }
    private void get_PointsHistory() {
        parseProfile();
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, user_id, ServerRequestConstants.Key_PostintValue);
                PostAsync mPostAsyncObj;
                if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                    mPostAsyncObj = new PostAsync(getActivity(), ShopHistory.this, ServerRequestConstants.DRIVER_SHOP_HISTORY,
                            mPostArrayList, ShopHistory.this);
                }else{
                    mPostAsyncObj = new PostAsync(getActivity(), ShopHistory.this, ServerRequestConstants.SHOP_HISTORY,
                            mPostArrayList, ShopHistory.this);
                }
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }
    private void initializeObjects() {
        TextView txttitle = (TextView) HomeScreen.toolbar.findViewById(R.id.title);
        txttitle.setText("Shop History");
        TextView  save = (TextView) HomeScreen.toolbar.findViewById(R.id.save);
        save.setVisibility(View.GONE);
        ImageView search = (ImageView) HomeScreen.toolbar.findViewById(R.id.search);
        search.setVisibility(View.GONE);
        shop_history=(RecyclerView)rootview.findViewById(R.id.point_history);
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        get_PointsHistory();

    }
    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(getActivity());
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(getActivity(),"Service Time out!!!","Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.SHOP_HISTORY)) {
            AppDelegate.save(getActivity(), result, Parameters.fan_ShopsHistory);
            parseFan_shopHistory();
        }else if (apiName.equals(ServerRequestConstants.DRIVER_SHOP_HISTORY)) {
            AppDelegate.save(getActivity(), result, Parameters.fan_ShopsHistory);
            parseFan_shopHistory();
        }

    }


    private void parseFan_shopHistory() {
String driver_response=AppDelegate.getValue(getActivity(),Parameters.fan_ShopsHistory);
        try {
            JSONObject jsonObject=new JSONObject(driver_response);
            productmodel=new ProductListModel();

            productmodel.setStatus(jsonObject.getInt("status"));

            if(productmodel.getStatus()==1){
                JSONObject object=jsonObject.getJSONObject("response");
                JSONArray jsonArray=object.getJSONArray("product_history");
                ArrayList<ProductListModel> shopHistoryArray=new ArrayList<>();
                for(int i=0;i<jsonArray.length();i++)
                {
                    JSONObject obj=jsonArray.getJSONObject(i);
                    productmodel=new ProductListModel();
                    productmodel.setProduct_id(obj.getInt("Product_id"));
                    productmodel.setFan_name(obj.getString("fan_name"));
                    productmodel.setProduct_name(obj.getString("Product_name"));
                    productmodel.setQuantity(obj.getInt("quantity"));
                    productmodel.setPrice(obj.getInt("price"));
                    productmodel.setImage(obj.getString("image"));
                    shopHistoryArray.add(productmodel);
                }
                setHistory(shopHistoryArray);
            }
            else if(productmodel.getStatus()==0){
                AppDelegate.ShowDialog(getActivity(),jsonObject.getString("message"),"Alert !!!");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setHistory(ArrayList<ProductListModel> pointsHistoryArray) {
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        ShophistoryAdapter adapter = new ShophistoryAdapter(getActivity(), pointsHistoryArray);
        shop_history.setLayoutManager(layoutManager);
        shop_history.setAdapter(adapter);
    }

    private void setdriverSponcer(ArrayList<SponcerModel> sponcerModelArrayList) {

         LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        SponcerBannerAdapter adapter = new SponcerBannerAdapter(getActivity(), sponcerModelArrayList);
        sponcer.setLayoutManager(layoutManager);
        sponcer.setAdapter(adapter);
    }

    private void parse_news() {
        String Citylist = AppDelegate.getValue(getActivity(), Parameters.get_news);
        try {
            JSONObject jsonObject = new JSONObject(Citylist);
            newsModel = new Get_News_Model();
            newsModel.setStatus(jsonObject.getInt("status"));
            newsModel.setDataflow(jsonObject.getInt("dataFlow"));
            if (newsModel.getStatus() == 1) {
                JSONObject jsonArray = jsonObject.getJSONObject("response");
                newsModel.setDescription(jsonArray.getString("Description"));
                news.setText(newsModel.getDescription() + "");
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }
    private void parseFan_List() {
        String Fan_list=AppDelegate.getValue(getActivity(),Parameters.fan_list);
        try {
            ArrayList<FanModel> fanList=new ArrayList<>();
            JSONObject object=new JSONObject(Fan_list);
            fanModel=new FanModel();
            fanModel.setStatus(object.getInt("status"));
            fanModel.setDataFlow(object.getInt("dataFlow"));
            if(fanModel.getStatus()==1){
                JSONObject response=object.getJSONObject("response");
                JSONArray FAN=response.getJSONArray("Fan List");
                for(int i=0; i<FAN.length();i++){
                    JSONObject FanDetail=FAN.getJSONObject(i);
                    fanModel=new FanModel();
                    fanModel.setFan_id(FanDetail.getInt("Fan_id"));
                    fanModel.setFan_name(FanDetail.getString("name"));
                    fanModel.setFan_image(FanDetail.getString("image"));
                    fanModel.setCreated(FanDetail.getString("created"));
                    fanList.add(fanModel);
                }
                setFanLIST(fanList);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void setFanLIST(ArrayList<FanModel> fanList) {
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        FanListAdapter adapter = new FanListAdapter(getActivity(), fanList);
        fanlist.setLayoutManager(layoutManager);
        fanlist.setAdapter(adapter);
    }
    private void showSponcer(final ArrayList<SponcerModel> sponcerModels) {
        /*LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        SponcerBannerAdapter adapter = new SponcerBannerAdapter(getActivity(), sponcerModels);
        banners.setLayoutManager(layoutManager);
        banners.setAdapter(adapter);*/
    }
}
