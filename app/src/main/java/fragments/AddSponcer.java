package fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.HomeScreen;
import com.noto.shorttrackhero.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import adapters.CategoryAdapter;
import adapters.CountryAdapter;
import adapters.ImageAdapter;
import adapters.StateAdapter;
import interfaces.OnListItemClickListener;
import interfaces.OnReciveServerResponse;
import model.CityListModel;
import model.CountryListModel;
import model.Driver;
import model.DriverDetailModel;
import model.FanModel;
import model.GetProfile_Model;
import model.Get_News_Model;
import model.LoginModel;
import model.PostAysnc_Model;
import model.ProductListModel;
import model.RaceDetailModel;
import model.SponcerCategoryModel;
import model.SponcerModel;
import model.StateListModel;

import static android.media.ExifInterface.ORIENTATION_NORMAL;
import static android.media.ExifInterface.TAG_ORIENTATION;

/**
 * Created by admin on 30-06-2016.
 */
public class AddSponcer extends Fragment implements OnReciveServerResponse, OnListItemClickListener, View.OnClickListener {
    private View rootview;

    private Bundle bundle;
    private RaceDetailModel racedetailModel;
    private int dotsCount, item_position = 0, selected_tab = 0, click_type = 0;
    private Driver driver;
    private LinearLayout pager_indicator;
    private RecyclerView fanlist;
    ImageView driver_img;
    TextView driver_name, fans, total_races, total_wins, rank;
    private FanModel fanModel;
    private int status;
    String message;
    private int user_id;
    private Get_News_Model newsModel;
    private TextView news;
    private SponcerModel sponcerModel;
    DriverDetailModel driverDetailModel;
    private RecyclerView sponcer;
    private GridView productlist;
    private ProductListModel shoplistModel;
    private ProductListModel product = new ProductListModel();
    private ImageAdapter imageAdapter;
    private ViewPager viewpager;
    private ImageView[] dots;
    private ArrayList<String> slider;
    private TextView name, price, description, buy, email_address, country, state, quantity;
    private int id;
    private EditText fullname, contact_no, zipcode, city;
    private CountryListModel countrylistitem;
    private ListView list;
    private int country_id;
    Dialog dialog;
    private TextView title;
    private GetProfile_Model getProfile_model;
    private String city_id;
    CityListModel citylistitem;
    private int state_id;
    StateListModel statelistitem;
    private ImageView image;
    private TextView chk_out;
    private EditText address;
    private String identifier;
    private TextView category, subcategory;
    private Bitmap OriginalPhoto;
    //    private DisplayImageOptions options;
    public static File capturedFile;
    public static Uri imageURI = null;
    public static String str_file_path = "";
    public static Uri imageUri;
    public static Uri cropimageUri;
    private TextView add;
    private SponcerCategoryModel categoryModel;
    private int category_id;
    private int subcategory_id;
    private String track_id;
    private LoginModel getProfile;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.add_sponcer, container, false);
        identifier = AppDelegate.getValue(getActivity(), Tags.IDENTIFIER);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
       /* AdView adView = (AdView) rootview.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .setRequestAgent("android_studio:ad_template").build();
        adView.loadAd(adRequest);*/

        initializeObjects();
        track_id = AppDelegate.getValue(getActivity(), Tags.TRACK_ID);
    }

    private void initializeObjects() {
        TextView txttitle = (TextView) HomeScreen.toolbar.findViewById(R.id.title);
        txttitle.setText("Add Sponsor");
        TextView save = (TextView) HomeScreen.toolbar.findViewById(R.id.save);
        save.setVisibility(View.GONE);
        ImageView search = (ImageView) HomeScreen.toolbar.findViewById(R.id.search);
        search.setVisibility(View.GONE);
        image = (ImageView) rootview.findViewById(R.id.upload_logo);
        name = (EditText) rootview.findViewById(R.id.sponcer_name);
        category = (TextView) rootview.findViewById(R.id.sponcer_category);
        subcategory = (TextView) rootview.findViewById(R.id.sponcer_subcategory);
        add = (TextView) rootview.findViewById(R.id.add);
        image.setOnClickListener(this);
        category.setOnClickListener(this);
        subcategory.setOnClickListener(this);
        add.setOnClickListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    private void setvalues() {
        String login = AppDelegate.getValue(getActivity(), Parameters.saveLogin);
        Log.e("login value", login + "");
        try {
            JSONObject json = new JSONObject(login);
            int status = json.getInt("status");
            if (status == 1) {
                JSONObject jsonObject = json.getJSONObject("response");
                id = jsonObject.getInt("id");
                AppDelegate.Log("user_id", id + "");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void Dialogue() {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        title = (TextView) dialog.findViewById(R.id.dialog_title);
        list = (ListView) dialog.findViewById(R.id.dialog_list);
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(getActivity());
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(getActivity(), "Service Time out!!!", "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.DRIVER_SPONCER_CATEGORY)) {
            AppDelegate.save(getActivity(), result, Parameters.sponcer_category);
            parse_sponcerCategory();
        } else if (apiName.equals(ServerRequestConstants.DRIVER_SPONCER_SUBCATEGORY)) {
            AppDelegate.save(getActivity(), result, Parameters.sponcer_subcategory);
            Log.e("sponcer_subcategory", result + "");
            parse_sponcerSubCategory();
        } else if (apiName.equals(ServerRequestConstants.DRIVER_ADD_SPONCER)) {
            AppDelegate.save(getActivity(), result, Parameters.add_Sponcer);
            Log.e("sponcer_subcategory", result + "");
            parse_addSponcer();
        }
    }

    private void parse_addSponcer() {
        String sponcercategory = AppDelegate.getValue(getActivity(), Parameters.add_Sponcer);
        try {
            ArrayList<SponcerCategoryModel> sponcerCategoryModelArrayList = new ArrayList<>();
            JSONObject obj = new JSONObject(sponcercategory);
            categoryModel = new SponcerCategoryModel();
            categoryModel.setStatus(obj.getInt("status"));
            if (categoryModel.getStatus() == 1) {
                AppDelegate.ShowDialog(getActivity(), "Sponsor added successfully.", "Alert!!!");
                name.setText("");
                category.setText("");
                subcategory.setText("");
                capturedFile=null;
            } else {
                AppDelegate.ShowDialog(getActivity(), "Bad response.", "Alert!!!");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void parse_sponcerSubCategory() {
        String sponcercategory = AppDelegate.getValue(getActivity(), Parameters.sponcer_subcategory);
        try {
            ArrayList<SponcerCategoryModel> sponcerCategoryModelArrayList = new ArrayList<>();
            JSONObject obj = new JSONObject(sponcercategory);
            categoryModel = new SponcerCategoryModel();
            categoryModel.setStatus(obj.getInt("status"));
            categoryModel.setDataflow(obj.getInt("dataFlow"));
            if (categoryModel.getStatus() == 1 && categoryModel.getDataflow() == 1) {
                JSONArray jsonArray = obj.getJSONArray("response");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    categoryModel = new SponcerCategoryModel();
                    categoryModel.setCategory_id(jsonObject.getInt("id"));
                    categoryModel.setName(jsonObject.getString("name"));
                    sponcerCategoryModelArrayList.add(categoryModel);
                }
                setsubsponcer(sponcerCategoryModelArrayList);
            } else {
                AppDelegate.ShowDialog(getActivity(), "No Sponsor found.", "Alert!!!");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setsubsponcer(final ArrayList<SponcerCategoryModel> sponcerCategoryModelArrayList) {
        Dialogue();

        CategoryAdapter spinnerAdapter = new CategoryAdapter(getActivity(), sponcerCategoryModelArrayList);
        list.setAdapter(spinnerAdapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String timee = sponcerCategoryModelArrayList.get(position).getName();
                subcategory.setText(timee);
                subcategory_id = sponcerCategoryModelArrayList.get(position).getCategory_id();
                dialog.dismiss();
            }
        });
        title.setText("Select Sponsor Subcategory");
        dialog.show();
    }

    private void parse_sponcerCategory() {
        String sponcercategory = AppDelegate.getValue(getActivity(), Parameters.sponcer_category);
        try {
            ArrayList<SponcerCategoryModel> sponcerCategoryModelArrayList = new ArrayList<>();
            JSONObject obj = new JSONObject(sponcercategory);
            categoryModel = new SponcerCategoryModel();
            categoryModel.setStatus(obj.getInt("status"));
            if (categoryModel.getStatus() == 1) {
                JSONArray jsonArray = obj.getJSONArray("response");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    categoryModel = new SponcerCategoryModel();
                    categoryModel.setCategory_id(jsonObject.getInt("id"));
                    categoryModel.setName(jsonObject.getString("name"));
                    sponcerCategoryModelArrayList.add(categoryModel);
                }
                setsponcer(sponcerCategoryModelArrayList);
            } else {
                AppDelegate.ShowDialog(getActivity(), "No Sponsor found.", "Alert!!!");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setsponcer(final ArrayList<SponcerCategoryModel> sponcerCategoryModelArrayList) {
        Dialogue();
        CategoryAdapter spinnerAdapter = new CategoryAdapter(getActivity(), sponcerCategoryModelArrayList);
        list.setAdapter(spinnerAdapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String timee = sponcerCategoryModelArrayList.get(position).getName();
                category.setText(timee);
                category_id = sponcerCategoryModelArrayList.get(position).getCategory_id();
                dialog.dismiss();
            }
        });
        title.setText("Select Sponsor Category ");
        dialog.show();
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        AppDelegate.LogT("setOnListItemClickListener => name = " + name + ", pos = " + position);
        this.item_position = position;
        this.click_type = 0;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sponcer_category:
                execute_categorylist();
                subcategory.setText("");

                break;
            case R.id.sponcer_subcategory:
                if (category.getText().toString().isEmpty()) {
                    AppDelegate.ShowDialog(getActivity(), "Please select Sponsor Category first", "Alert!!!");
                } else {
                    execute_subCategorylist();

                }
                break;
            case R.id.upload_logo:
                showImageSelectorList();
                break;
            case R.id.add:
                boolean result = validate();
                if (result == true) {
                    add();
                }
                break;
        }
    }

    public void showImageSelectorList() {
        AppDelegate.hideKeyBoard(getActivity());
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        ListView modeList = new ListView(getActivity());
        String[] stringArray = new String[]{"Camera", "Gallery", "Cancel"};
        ArrayAdapter<String> modeAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, stringArray);
        modeList.setAdapter(modeAdapter);
        builder.setView(modeList);
        final Dialog dialog = builder.create();
        modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        dialog.dismiss();
                        new OpenCamera().execute();
                        break;
                    case 1:
                        dialog.dismiss();
                        openGallery();
                        break;
                    case 2:
                        dialog.dismiss();
                        break;
                }
            }
        });
        dialog.show();
    }

    public void openGallery() {
        startActivityForResult(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI), AppDelegate.SELECT_PICTURE);
    }

    class OpenCamera extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            AppDelegate.showProgressDialog(getActivity());
        }

        @Override
        protected Void doInBackground(Void... params) {
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            String str_file_path = getNewFile();
            if (str_file_path == null ? true : str_file_path.length() > 0 ? false : true) {
                AppDelegate.showToast(getActivity(), "File not created, please try agin later.");
                return null;
            }
            imageURI = Uri.fromFile(capturedFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
            startActivityForResult(intent, AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            AppDelegate.hideProgressDialog(getActivity());
        }
    }

    public String getNewFile() {
        File directoryFile;
        if (AppDelegate.isSDcardAvailable()) {
            directoryFile = new File(Environment.getExternalStorageDirectory()
                    + "/" + getString(R.string.app_name));
        } else {
            directoryFile = getActivity().getDir(getString(R.string.app_name), Context.MODE_PRIVATE);
        }
        if (directoryFile.exists() && directoryFile.isDirectory()
                || directoryFile.mkdirs()) {
            capturedFile = new File(directoryFile, "Image_" + System.currentTimeMillis()
                    + ".png");
            try {
                if (capturedFile.createNewFile()) {
                    AppDelegate.LogT("File created = "
                            + capturedFile.getAbsolutePath());
                    return capturedFile.getAbsolutePath();
                }
            } catch (IOException e) {
                AppDelegate.LogE(e);
            }
        }
        AppDelegate.LogE("no file created.");
        return null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        AppDelegate.LogT("onActivityResult Profile");
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case AppDelegate.SELECT_PICTURE:
                if (resultCode == Activity.RESULT_OK) {
                    Uri uri = data.getData();
                    imageUri = uri;
                    AppDelegate.LogT("uri=" + uri);
                    if (uri != null) {
                        // User had pick an image.
                        Cursor cursor = getActivity()
                                .getContentResolver()
                                .query(uri,
                                        new String[]{MediaStore.Images.ImageColumns.DATA},
                                        null, null, null);
                        cursor.moveToFirst();
                        // Link to the image
                        final String imageFilePath = cursor.getString(0);
                        cursor.close();
                        int orientation = 0;
                        if (imageFilePath != null && !imageFilePath.equalsIgnoreCase("")) {
                            try {
                                ExifInterface ei = new ExifInterface(imageFilePath);
                                orientation = ei.getAttributeInt(
                                        TAG_ORIENTATION,
                                        ORIENTATION_NORMAL);
                            } catch (IOException e) {
                                AppDelegate.LogE(e);
                            }
                            capturedFile = new File(imageFilePath);
                            OriginalPhoto = AppDelegate.decodeFile(capturedFile);

                            Log.d("image", "orientation is " + orientation);
                            Matrix matrix = new Matrix();

                            switch (orientation) {
                                case ExifInterface.ORIENTATION_NORMAL:
                                    System.out.println("ORIENTATION_NORMAL" + orientation);
                                    break;
                                case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                                    matrix.setScale(-1, 1);
                                    System.out.println("ORIENTATION_FLIP_HORIZONTAL" + orientation);

                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_180:
                                    matrix.setRotate(180);
                                    break;
                                case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                                    matrix.setRotate(180);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_TRANSPOSE:
                                    matrix.setRotate(90);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_90:
                                    matrix.setRotate(90);
                                    break;
                                case ExifInterface.ORIENTATION_TRANSVERSE:
                                    matrix.setRotate(-90);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_270:
                                    matrix.setRotate(-90);
                                    break;
                                default:
                                    break;

                            }
                            Log.d("OriginalPhoto", "after OriginalPhoto is " + OriginalPhoto);
                            if (OriginalPhoto != null) {
                                OriginalPhoto = Bitmap.createBitmap(OriginalPhoto, 0, 0, OriginalPhoto.getWidth(),
                                        OriginalPhoto.getHeight(), matrix, true);
                                Uri rotateduri = getImageUri(getActivity(), OriginalPhoto);
                                if (rotateduri != null) {
                                    if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP)
                                        performCrop(rotateduri);
                                    else {
                                        this.OriginalPhoto = Bitmap.createScaledBitmap(OriginalPhoto, 240, 240, true);
                                        // image.setImageBitmap(OriginalPhoto);
                                    }
                                    //performCrop(rotateduri);
                                } else {
                                    if (imageUri != null)
                                        performCrop(imageUri);
                                    else
                                        Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(getActivity(), "Failed to deliver result.", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                }
                break;
            case AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE:
                AppDelegate.LogT("onActivityResult str_file_path = " + str_file_path + ", imageUri = " + imageUri);
                //Get our saved file into a bitmap object:
                try {
                    if (resultCode == Activity.RESULT_OK) {
                        int orientation = 0;
                        Uri uri = Uri.fromFile(new File(capturedFile.getAbsolutePath()));

                        BitmapFactory.Options o = new BitmapFactory.Options();
                        o.inJustDecodeBounds = true;
                        OriginalPhoto = AppDelegate.decodeSampledBitmapFromFile(capturedFile.getAbsolutePath(), 1000, 700);
                        Log.d("OriginalPhoto", "OriginalPhoto is " + OriginalPhoto);
                        try {
                            ExifInterface ei = new ExifInterface(
                                    uri.getPath());

                            orientation = ei.getAttributeInt(
                                    ExifInterface.TAG_ORIENTATION,
                                    ExifInterface.ORIENTATION_NORMAL);

                            Log.d("image", "orientation is " + orientation);
                            Matrix matrix = new Matrix();

                            switch (orientation) {
                                case ExifInterface.ORIENTATION_NORMAL:
                                    System.out.println("ORIENTATION_NORMAL" + orientation);
                                    break;
                                case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                                    matrix.setScale(-1, 1);
                                    System.out.println("ORIENTATION_FLIP_HORIZONTAL" + orientation);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_180:
                                    matrix.setRotate(180);
                                    break;
                                case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                                    matrix.setRotate(180);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_TRANSPOSE:
                                    matrix.setRotate(90);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_90:
                                    matrix.setRotate(90);
                                    break;
                                case ExifInterface.ORIENTATION_TRANSVERSE:
                                    matrix.setRotate(-90);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_270:
                                    matrix.setRotate(-90);
                                    break;
                            }
                            Log.d("OriginalPhoto", "after OriginalPhoto is " + OriginalPhoto);
                            OriginalPhoto = Bitmap.createBitmap(OriginalPhoto, 0, 0, OriginalPhoto.getWidth(),
                                    OriginalPhoto.getHeight(), matrix, true);

                            Uri rotateduri = getImageUri(getActivity(), OriginalPhoto);
                            if (rotateduri != null) {
                                // performCrop(rotateduri);
                                if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP)
                                    performCrop(rotateduri);
                                else {
                                    this.OriginalPhoto = Bitmap.createScaledBitmap(OriginalPhoto, 240, 240, true);
                                    // image.setImageBitmap(OriginalPhoto);
                                }
                            } else {
                                AppDelegate.showToast(getActivity(), "Failure delivery result, please try again later.");
                            }

                        } catch (Exception e) {
                            AppDelegate.LogE(e);
                            AppDelegate.showToast(getActivity(), "Failure delivery result, please try again later.");
                        }
                    }
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
                break;
            case AppDelegate.PIC_CROP: {
                AppDelegate.LogT("at onActivicTyResult cropimageUri = " + cropimageUri);
                try {
                    cropimageUri = data.getData();
                    if (resultCode == Activity.RESULT_OK) {
                        if (OriginalPhoto != null) {
                            OriginalPhoto.recycle();
                            OriginalPhoto = null;
                        }
                        OriginalPhoto = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), cropimageUri);
                        AppDelegate.LogT("at onActivicTyResult selectedBitmap = " + OriginalPhoto);
                        OriginalPhoto = Bitmap.createScaledBitmap(
                                OriginalPhoto, 240, 240, true);
                        AppDelegate.LogT("at onActivicTyResult OriginalPhoto = " + OriginalPhoto);

                        // image.setImageBitmap(AppDelegate.getRoundedCornerBitmap(OriginalPhoto, AppDelegate.convertdp(getActivity(), 100)));

                        capturedFile = new File(getNewFile());
                        FileOutputStream fOut = null;
                        try {
                            fOut = new FileOutputStream(capturedFile);
                        } catch (FileNotFoundException e) {
                            AppDelegate.LogE(e);
                        }
                        OriginalPhoto.compress(Bitmap.CompressFormat.PNG, 85, fOut);
                        try {
                            fOut.flush();
                        } catch (IOException e) {
                            AppDelegate.LogE(e);
                        }
                        try {
                            fOut.close();
                        } catch (IOException e) {
                            AppDelegate.LogE(e);
                        }

                        // getActivity().getContentResolver().delete(cropimageUri, null, null);
                    } else {
                        AppDelegate.LogE("at onActivicTyResult failed to crop");
                    }
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
            }
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "CropTitle", null);
        return Uri.parse(path);
    }

    private void performCrop(Uri picUri) {
        try {
            ContentValues values = new ContentValues();

            values.put(MediaStore.Images.Media.TITLE, "cropuser");

            values.put(MediaStore.Images.Media.DESCRIPTION, "cropuserPic");

            cropimageUri = getActivity().getContentResolver().insert(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("scale", true);

            // indicate output X and Y
            cropIntent.putExtra("outputX", 1000);
            cropIntent.putExtra("outputY", 1000);

            // retrieve data on return
            cropIntent.putExtra("return-data", false);
//            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, cropimageUri);
            // start the activity - we handle returning in onActivityResult
            AppDelegate.LogT("at performCrop cropimageUri = " + cropimageUri + ", picUri = " + picUri);
            startActivityForResult(cropIntent, AppDelegate.PIC_CROP);
        } catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private void execute_subCategorylist() {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.category_id, category_id, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                PostAsync mPostAsyncObj;
                if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                    mPostAsyncObj = new PostAsync(getActivity(), AddSponcer.this, ServerRequestConstants.DRIVER_SPONCER_SUBCATEGORY,
                            mPostArrayList, AddSponcer.this);
                    AppDelegate.showProgressDialog(getActivity());
                    mPostAsyncObj.execute();
                }
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    private void execute_categorylist() {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                PostAsync mPostAsyncObj;
                if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                    mPostAsyncObj = new PostAsync(getActivity(), AddSponcer.this, ServerRequestConstants.DRIVER_SPONCER_CATEGORY,
                            mPostArrayList, AddSponcer.this);
                    AppDelegate.showProgressDialog(getActivity());
                    mPostAsyncObj.execute();
                }
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    private void parseProfile() {
        String Register = AppDelegate.getValue(getActivity(), Parameters.saveLogin);
        AppDelegate.Log("response", Register + "");
        try {
            JSONObject jsonObject = new JSONObject(Register);
            status = jsonObject.getInt("status");
            getProfile = new LoginModel();
            if (status == 1) {
                JSONObject response = jsonObject.getJSONObject("response");
                getProfile.setId(response.getInt("id"));
                user_id = getProfile.getId();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void add() {
        try {
            parseProfile();
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.category_id, category_id, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.subcat_id, subcategory_id, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.uploadedfile0, capturedFile, ServerRequestConstants.Key_PostFileValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.name, name.getText().toString() + "");
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, user_id, ServerRequestConstants.Key_PostintValue);

                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.track_id, track_id, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                PostAsync mPostAsyncObj;
                if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                    mPostAsyncObj = new PostAsync(getActivity(), AddSponcer.this, ServerRequestConstants.DRIVER_ADD_SPONCER,
                            mPostArrayList, AddSponcer.this);
                    AppDelegate.showProgressDialog(getActivity());
                    mPostAsyncObj.execute();
                } else {
                }

            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    private boolean validate() {
        boolean validate = true;
        AppDelegate.Log("capture file check",capturedFile+""+ "addd");
        try {
            if (name.getText().toString().isEmpty() && category.getText().toString().isEmpty() && subcategory.getText().toString().isEmpty() && capturedFile == null) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Please fill the fields", "Alert!!!");
            } else if (name.getText().toString().isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), " Please Enter  name", "Alert!!!");
            } else if (category.getText().toString().isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Please  select category.", "Alert!!!");
            } else if (subcategory.getText().toString().isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Please  select subcategory", "Alert!!!");
            } else if (capturedFile == null) {
                AppDelegate.Log("capture file",capturedFile+""+ "addd");
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Please select Sponsor Logo", "Alert!!!");
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        return validate;
    }

}
