package fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.HomeScreen;
import com.noto.shorttrackhero.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import adapters.ImageAdapter;
import adapters.ProductListAdapter;
import interfaces.OnListItemClickListener;
import interfaces.OnReciveServerResponse;
import model.Driver;
import model.DriverDetailModel;
import model.FanModel;
import model.Get_News_Model;
import model.LoginModel;
import model.PostAysnc_Model;
import model.ProductListModel;
import model.RaceDetailModel;
import model.SponcerModel;

/**
 * Created by admin on 30-06-2016.
 */
public class ShopDetail extends Fragment implements OnReciveServerResponse, OnListItemClickListener, View.OnClickListener {
    private View rootview;

    private Bundle bundle;
    private RaceDetailModel racedetailModel;
    private int dotsCount, item_position = 0, selected_tab = 0, click_type = 0;
    private Driver driver;
    private android.widget.LinearLayout pager_indicator;
    private RecyclerView fanlist;
    ImageView driver_img;
    TextView driver_name, fans, total_races, total_wins, rank;
    private FanModel fanModel;
    private int status;
    LoginModel getProfile_model;
    private int user_id;
    private Get_News_Model newsModel;
    private TextView news;
    private SponcerModel sponcerModel;
    DriverDetailModel driverDetailModel;
    private RecyclerView sponcer;
    private GridView productlist;
    private ProductListModel shoplistModel;
    private ProductListModel product;
    private ImageAdapter imageAdapter;
    private ViewPager viewpager;
    private ImageView[] dots;
    private ArrayList<String> slider;
    private TextView name, price, description, buy;
    private ArrayList<ProductListModel> productListModelArrayList;
    private String identifier;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.shop_detail, container, false);
        identifier = AppDelegate.getValue(getActivity(), Tags.IDENTIFIER);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        AdView adView = (AdView) rootview.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .setRequestAgent("android_studio:ad_template").build();
        adView.loadAd(adRequest);
        initializeObjects();
    }

    private void initializeObjects() {
        TextView txttitle = (TextView) HomeScreen.toolbar.findViewById(R.id.title);
        txttitle.setText("Shop Detail");
        TextView save = (TextView) HomeScreen.toolbar.findViewById(R.id.save);
        save.setVisibility(View.GONE);
        ImageView search = (ImageView) HomeScreen.toolbar.findViewById(R.id.search);
        search.setVisibility(View.GONE);
        bundle = getArguments();
        product = bundle.getParcelable(Tags.parcel_ProductDetail);
        viewpager = (ViewPager) rootview.findViewById(R.id.image);
        pager_indicator = (android.widget.LinearLayout) rootview.findViewById(R.id.pager_indicator);
        name = (TextView) rootview.findViewById(R.id.name);
        price = (TextView) rootview.findViewById(R.id.price);
        description = (TextView) rootview.findViewById(R.id.description);
        buy = (TextView) rootview.findViewById(R.id.buy_item);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getproductdetail();
    }

    private void getproductdetail() {
        try {
            {
                if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                    ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.id, product.getProduct_id(), ServerRequestConstants.Key_PostintValue);
                    PostAsync mPostAsyncObj;
                    if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                        mPostAsyncObj = new PostAsync(getActivity(), ShopDetail.this, ServerRequestConstants.DRIVER_PRODUCT_DETAIL,
                                mPostArrayList, ShopDetail.this);
                    } else {
                        mPostAsyncObj = new PostAsync(getActivity(), ShopDetail.this, ServerRequestConstants.PRODUCT_DETAIL,
                                mPostArrayList, ShopDetail.this);
                    }
                    AppDelegate.showProgressDialog(getActivity());
                    mPostAsyncObj.execute();
                }
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(getActivity());
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(getActivity(), "Service Time out!!!", "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.PRODUCT_DETAIL)) {
            AppDelegate.save(getActivity(), result, Parameters.product_detail);
            parseProduct_detail();
        }
        if (apiName.equals(ServerRequestConstants.DRIVER_PRODUCT_DETAIL)) {
            AppDelegate.save(getActivity(), result, Parameters.product_detail);
            parseProduct_detail();
        }
    }

    private void parseProduct_detail() {
        String shop_list = AppDelegate.getValue(getActivity(), Parameters.product_detail);
        try {
            JSONObject obj = new JSONObject(shop_list);
            shoplistModel = new ProductListModel();
            shoplistModel.setStatus(obj.getInt("status"));
            productListModelArrayList = new ArrayList<>();
            shoplistModel.setDataflow(obj.getInt("dataFlow"));
            if (shoplistModel.getStatus() == 1) {
                JSONArray response = obj.getJSONArray("response");
                for (int i = 0; i < response.length(); i++) {
                    JSONObject jsonObject = response.getJSONObject(i);
                    shoplistModel = new ProductListModel();
                    shoplistModel.setProduct_id(jsonObject.getInt("id"));
                    shoplistModel.setProduct_name(jsonObject.getString("name"));
                    shoplistModel.setDescription(jsonObject.getString("description"));
                    shoplistModel.setPrice(jsonObject.getDouble("price"));
                    shoplistModel.setQuantityString(jsonObject.getString("quantity"));
                    shoplistModel.setImage1(jsonObject.getString("image1"));
                    shoplistModel.setImage2(jsonObject.getString("image2"));
                    shoplistModel.setImage3(jsonObject.getString("image3"));
                    shoplistModel.setImage4(jsonObject.getString("image4"));
                    productListModelArrayList.add(shoplistModel);
                }
                setProductList(productListModelArrayList);
                setproductdetails(productListModelArrayList);
            } else {
                AppDelegate.ShowDialog(getActivity(), "No record found.", "Alert!!!");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setproductdetails(ArrayList<ProductListModel> productListModelArrayList) {
        for (ProductListModel product : productListModelArrayList) {
            name.setText(product.getProduct_name());
            price.setText("Credit :" + String.valueOf(product.getPrice()));
            description.setText(product.getDescription());
            buy.setOnClickListener(this);
        }
    }

    private void setProductList(ArrayList<ProductListModel> productListModelArrayList) {
        slider = new ArrayList<>();
        for (ProductListModel product : productListModelArrayList) {
            slider.add(product.getImage1());
            slider.add(product.getImage2());
            slider.add(product.getImage3());
            slider.add(product.getImage4());
        }
        imageAdapter = new ImageAdapter(slider, ShopDetail.this);
        viewpager.setAdapter(imageAdapter);
        setUiPageViewController();
    }

    private void setUiPageViewController() {
        pager_indicator.removeAllViews();
        dotsCount = imageAdapter.getCount();
        if (dotsCount > 0) {
            dots = new ImageView[dotsCount];
            for (int i = 0; i < dotsCount; i++) {
                dots[i] = new ImageView(getActivity());
                dots[i].setImageDrawable(getResources().getDrawable(R.drawable.white_radius_square));
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(25, 25);
                params.setMargins(10, 0, 10, 0);
                pager_indicator.addView(dots[i], params);
            }
            dots[0].setImageDrawable(getResources().getDrawable(R.drawable.yellow_radius_square));
        }
        next();
    }

    private void next() {
        viewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switchBannerPage(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        switchBannerPage(0);
    }

    private void switchBannerPage(int position) {
        if (dotsCount > 0) {
            for (int i = 0; i < dotsCount; i++) {
                dots[i].setImageResource(R.drawable.white_radius_square);
            }
            dots[position].setImageResource(R.drawable.yellow_radius_square);
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        AppDelegate.LogT("setOnListItemClickListener => name = " + name + ", pos = " + position);
        this.item_position = position;
        this.click_type = 0;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buy_item:
                bundle.putParcelable(Tags.parcel_ProductDetail, productListModelArrayList.get(0));
                AppDelegate.showFragment(getActivity(), new CheckOut(), R.id.framelayout, bundle, null);
                break;
        }

    }
}
