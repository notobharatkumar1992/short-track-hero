package fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.HomeScreen;
import com.noto.shorttrackhero.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import adapters.FanListAdapter;
import adapters.PointshistoryAdapter;
import adapters.SponcerBannerAdapter;
import interfaces.OnReciveServerResponse;
import model.Driver;
import model.DriverDetailModel;
import model.FanModel;
import model.Get_News_Model;
import model.LoginModel;
import model.PointsHistoryModel;
import model.PostAysnc_Model;
import model.RaceDetailModel;
import model.SponcerModel;

/**
 * Created by admin on 30-06-2016.
 */
public class PointsHistory extends Fragment implements OnReciveServerResponse {
    private View rootview;

    private Bundle bundle;
    private RaceDetailModel racedetailModel;

    private Driver driver;

    private RecyclerView fanlist;
    ImageView driver_img;
    TextView driver_name, fans, total_races, total_wins, rank;
    private FanModel fanModel;
    private int status;
    LoginModel getProfile_model;
    private int user_id;
    private Get_News_Model newsModel;
    private TextView news;
    private SponcerModel sponcerModel;
    DriverDetailModel driverDetailModel;
    private RecyclerView sponcer;
    private RecyclerView point_history;
    private PointsHistoryModel point_historyModel;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.point_history, container, false);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeObjects();
    }

    private void parseProfile() {
        String Register = AppDelegate.getValue(getActivity(), Parameters.saveLogin);
        AppDelegate.Log("response", Register + "");
        try {
            JSONObject jsonObject = new JSONObject(Register);
            status = jsonObject.getInt("status");
            getProfile_model = new LoginModel();
            if (status == 1) {
                JSONObject response = jsonObject.getJSONObject("response");
                getProfile_model.setId(response.getInt("id"));
                user_id = getProfile_model.getId();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void get_PointsHistory() {
        parseProfile();
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, user_id, ServerRequestConstants.Key_PostintValue);
                PostAsync mPostAsyncObj = new PostAsync(getActivity(), PointsHistory.this, ServerRequestConstants.FAN_POINTS,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    private void initializeObjects() {
        TextView txttitle = (TextView) HomeScreen.toolbar.findViewById(R.id.title);
        txttitle.setText("Points History");
        TextView save = (TextView) HomeScreen.toolbar.findViewById(R.id.save);
        save.setVisibility(View.GONE);
        ImageView search = (ImageView) HomeScreen.toolbar.findViewById(R.id.search);
        search.setVisibility(View.GONE);
        point_history = (RecyclerView) rootview.findViewById(R.id.point_history);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        get_PointsHistory();
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(getActivity());
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(getActivity(),"Time out!!!Please Try Again!","Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.FAN_POINTS)) {
            AppDelegate.save(getActivity(), result, Parameters.fan_pointsHistory);
            parseFan_pointsHistory();
        }
    }

    private void parseFan_pointsHistory() {
        String driver_response = AppDelegate.getValue(getActivity(), Parameters.fan_pointsHistory);
        try {
            JSONObject jsonObject = new JSONObject(driver_response);
            point_historyModel = new PointsHistoryModel();

            point_historyModel.setStatus(jsonObject.getInt("status"));

            if (point_historyModel.getStatus() == 1) {
                JSONObject object = jsonObject.getJSONObject("response");
                JSONArray jsonArray = object.getJSONArray("point_history");
                ArrayList<PointsHistoryModel> pointsHistoryArray = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject obj = jsonArray.getJSONObject(i);
                    point_historyModel = new PointsHistoryModel();
                    point_historyModel.setId(obj.getInt("id"));
                    point_historyModel.setDriver_name(obj.getString("driver_name"));
                    point_historyModel.setFan_name(obj.getString("fan_name"));
                    point_historyModel.setTrack_name(obj.getString("track_name"));
                    point_historyModel.setCompetitions_name(obj.getString("competitions_name"));
                    point_historyModel.setRace_name(obj.getString("race_name"));
                    point_historyModel.setDate(obj.getString("date"));
                    point_historyModel.setTime(obj.getString("time"));
                    point_historyModel.setLap(obj.getString("lap"));
                    point_historyModel.setAddress(obj.getString("address"));
                    point_historyModel.setCurrent_rank(obj.getString("current_rank"));
                    point_historyModel.setPosition(obj.getInt("position"));
                    point_historyModel.setTotal_credit(obj.getInt("total_credit"));
                    point_historyModel.setTotal_point(obj.getInt("total_point"));
                    pointsHistoryArray.add(point_historyModel);
                }
                setHistory(pointsHistoryArray);
            } else if (point_historyModel.getStatus() == 0) {
                AppDelegate.ShowDialog(getActivity(), jsonObject.getString("message"), "Alert !!!");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setHistory(ArrayList<PointsHistoryModel> pointsHistoryArray) {
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        PointshistoryAdapter adapter = new PointshistoryAdapter(getActivity(), pointsHistoryArray);
        point_history.setLayoutManager(layoutManager);
        point_history.setAdapter(adapter);
    }

    private void setdriverSponcer(ArrayList<SponcerModel> sponcerModelArrayList) {

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        SponcerBannerAdapter adapter = new SponcerBannerAdapter(getActivity(), sponcerModelArrayList);
        sponcer.setLayoutManager(layoutManager);
        sponcer.setAdapter(adapter);
    }

    private void parse_news() {
        String Citylist = AppDelegate.getValue(getActivity(), Parameters.get_news);
        try {
            JSONObject jsonObject = new JSONObject(Citylist);
            newsModel = new Get_News_Model();
            newsModel.setStatus(jsonObject.getInt("status"));
            newsModel.setDataflow(jsonObject.getInt("dataFlow"));
            if (newsModel.getStatus() == 1) {
                JSONObject jsonArray = jsonObject.getJSONObject("response");
                newsModel.setDescription(jsonArray.getString("Description"));
                news.setText(newsModel.getDescription() + "");
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void parseFan_List() {
        String Fan_list = AppDelegate.getValue(getActivity(), Parameters.fan_list);
        try {
            ArrayList<FanModel> fanList = new ArrayList<>();
            JSONObject object = new JSONObject(Fan_list);
            fanModel = new FanModel();
            fanModel.setStatus(object.getInt("status"));
            fanModel.setDataFlow(object.getInt("dataFlow"));
            if (fanModel.getStatus() == 1) {
                JSONObject response = object.getJSONObject("response");
                JSONArray FAN = response.getJSONArray("Fan List");
                for (int i = 0; i < FAN.length(); i++) {
                    JSONObject FanDetail = FAN.getJSONObject(i);
                    fanModel = new FanModel();
                    fanModel.setFan_id(FanDetail.getInt("Fan_id"));
                    fanModel.setFan_name(FanDetail.getString("name"));
                    fanModel.setFan_image(FanDetail.getString("image"));
                    fanModel.setCreated(FanDetail.getString("created"));
                    fanList.add(fanModel);
                }
                setFanLIST(fanList);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setFanLIST(ArrayList<FanModel> fanList) {
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        FanListAdapter adapter = new FanListAdapter(getActivity(), fanList);
        fanlist.setLayoutManager(layoutManager);
        fanlist.setAdapter(adapter);
    }

    private void showSponcer(final ArrayList<SponcerModel> sponcerModels) {
        /*LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        SponcerBannerAdapter adapter = new SponcerBannerAdapter(getActivity(), sponcerModels);
        banners.setLayoutManager(layoutManager);
        banners.setAdapter(adapter);*/
    }
}
