package fragments;

import android.support.v4.app.Fragment;

import com.noto.shorttrackhero.AppDelegate;

import org.json.JSONObject;

import java.util.ArrayList;

import Async.PostAsync;
import Constants.ServerRequestConstants;
import Constants.Tags;
import interfaces.OnReciveServerResponse;
import model.PostAysnc_Model;
/**
 * Created by admin on 15-06-2016.
 */
public class DemoFragment extends Fragment implements OnReciveServerResponse {
    private void execute_resetPasswordAsync() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.address, "");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.address, "", ServerRequestConstants.Key_PostFileValue);

            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    DemoFragment.this, ServerRequestConstants.RESET_PASSWORD,
                    mPostArrayList, DemoFragment.this);
            AppDelegate.showProgressDialog(getActivity());
            mPostAsyncObj.execute();
        }
    }
    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(getActivity());
        if (apiName.equals(ServerRequestConstants.RESET_PASSWORD)) {
            parseDemoResponse(result);
        }
    }
    private void parseDemoResponse(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }
}
