package fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.HomeScreen;
import com.noto.shorttrackhero.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import adapters.NotificationAdapter;
import adapters.ProductListAdapter;
import interfaces.OnReciveServerResponse;
import model.Driver;
import model.DriverDetailModel;
import model.FanModel;
import model.Get_News_Model;
import model.LoginModel;
import model.NotificationModel;
import model.PostAysnc_Model;
import model.ProductListModel;
import model.RaceDetailModel;
import model.SponcerModel;

/**
 * Created by admin on 30-06-2016.
 */
public class Notification extends Fragment implements OnReciveServerResponse {
    private View rootview;
    private Bundle bundle = new Bundle();
    private RaceDetailModel racedetailModel;
    private Driver driver;
    private RecyclerView fanlist;
    ImageView driver_img;
    TextView driver_name, fans, total_races, total_wins, rank;
    private FanModel fanModel;
    private int status;
    LoginModel getProfile_model;
    private int user_id;
    private Get_News_Model newsModel;
    private TextView news;
    private SponcerModel sponcerModel;
    DriverDetailModel driverDetailModel;
    private RecyclerView sponcer;
    private GridView productlist;
    private NotificationModel shoplistModel;
    private static final String TOAST_TEXT = "Test ads are being shown. "
            + "To show live ads, replace the ad unit ID in res/values/strings.xml with your own ad unit ID.";
    private String identifier;
    private ListView notification;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.notifications, container, false);
        identifier = AppDelegate.getValue(getActivity(), Tags.IDENTIFIER);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeObjects();
        get_news();
      /*  AdView adView = (AdView)rootview. findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .setRequestAgent("android_studio:ad_template").build();
        adView.loadAd(adRequest);*/
    }
    private void get_news() {
        parseProfile();
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, user_id, ServerRequestConstants.Key_PostintValue);
                PostAsync mPostAsyncObj;
                if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                    mPostAsyncObj = new PostAsync(getActivity(), Notification.this, ServerRequestConstants.DRIVER_GET_NEWS,
                            mPostArrayList, null);
                } else {
                    mPostAsyncObj = new PostAsync(getActivity(), Notification.this, ServerRequestConstants.GET_NEWS,
                            mPostArrayList, null);
                }
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }
    private void initializeObjects() {
        TextView txttitle = (TextView) HomeScreen.toolbar.findViewById(R.id.title);
        txttitle.setText("Notification");
        TextView save = (TextView) HomeScreen.toolbar.findViewById(R.id.save);
        save.setVisibility(View.GONE);
        ImageView search = (ImageView) HomeScreen.toolbar.findViewById(R.id.search);
        search.setVisibility(View.GONE);
        notification = (ListView) rootview.findViewById(R.id.notifications);
        news = (TextView) rootview.findViewById(R.id.news);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getnotification();
    }

    private void parseProfile() {
        String Register = AppDelegate.getValue(getActivity(), Parameters.saveLogin);
        AppDelegate.Log("response", Register + "");
        try {
            JSONObject jsonObject = new JSONObject(Register);
            status = jsonObject.getInt("status");
            getProfile_model = new LoginModel();
            if (status == 1) {
                JSONObject response = jsonObject.getJSONObject("response");
                getProfile_model.setId(response.getInt("id"));
                user_id = getProfile_model.getId();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void getnotification() {
        parseProfile();
        try {
            {
                if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                    ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, user_id, ServerRequestConstants.Key_PostintValue);
                    PostAsync mPostAsyncObj;
                    if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                        mPostAsyncObj = new PostAsync(getActivity(), Notification.this, ServerRequestConstants.DRIVER_NOTIFICATION,
                                mPostArrayList, Notification.this);
                    } else {
                        mPostAsyncObj = new PostAsync(getActivity(), Notification.this, ServerRequestConstants.NOTIFICATION,
                                mPostArrayList, Notification.this);
                    }
                    AppDelegate.showProgressDialog(getActivity());
                    mPostAsyncObj.execute();
                }
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(getActivity());
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(getActivity(), "Service Time out!!!", "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.DRIVER_NOTIFICATION)) {
            AppDelegate.save(getActivity(), result, Parameters.notification);
            parseProduct_List();
        } else if (apiName.equals(ServerRequestConstants.NOTIFICATION)) {
            AppDelegate.save(getActivity(), result, Parameters.notification);
            parseProduct_List();
        }else if (apiName.equals(ServerRequestConstants.GET_NEWS)) {
            AppDelegate.save(getActivity(), result, Parameters.get_news);
            AppDelegate.Log("NEWS", result);
            parse_news();
        } else if (apiName.equals(ServerRequestConstants.DRIVER_GET_NEWS)) {
            AppDelegate.save(getActivity(), result, Parameters.get_news);
            AppDelegate.Log("NEWS", result);
            parse_news();
        }
    }
    private void parse_news() {
        String Citylist = AppDelegate.getValue(getActivity(), Parameters.get_news);
        try {
            JSONObject jsonObject = new JSONObject(Citylist);
            newsModel = new Get_News_Model();
            newsModel.setStatus(jsonObject.getInt("status"));
            newsModel.setDataflow(jsonObject.getInt("dataFlow"));
            if (newsModel.getStatus() == 1) {
                JSONObject jsonArray = jsonObject.getJSONObject("response");
                newsModel.setDescription(jsonArray.getString("Description"));
               // news.setText(newsModel.getDescription() + "");
                SpannableStringBuilder ssb = new SpannableStringBuilder("");
                String[] newsArray = (newsModel.getDescription() + "").split("\\%", -1);
                int counter = 0;
                Log.d("test", "newsArray" + newsArray[0] + "news => " + newsArray.length);
                for (int i = 0; i <= newsArray.length - 1; i++) {
                    if (newsArray[i].length() > 0) {
                        Log.d("test", "text => " + newsArray[i] + " , " + newsArray[i].length() + ", i length = " + i + ", counter = " + counter);
                        ssb.append(newsArray[i] + " ");
                        counter += newsArray[i].length() + 1;
                        Bitmap smiley = BitmapFactory.decodeResource(getResources(), R.drawable.flag);
                        try {
                            if (counter > 0)
                                ssb.setSpan(new ImageSpan(smiley, ImageSpan.ALIGN_BASELINE), counter - 1, counter, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                news.setSelected(true);
                news.setText(ssb);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void parseProduct_List() {
        String shop_list = AppDelegate.getValue(getActivity(), Parameters.notification);
        try {
            JSONObject obj = new JSONObject(shop_list);
            shoplistModel = new NotificationModel();
            shoplistModel.setStatus(obj.getInt("status"));
            ArrayList<NotificationModel> productListModelArrayList = new ArrayList<>();
            shoplistModel.setDataflow(obj.getInt("dataFlow"));
            if (shoplistModel.getStatus() == 1) {
                JSONArray response = obj.getJSONArray("response");
                for (int i = 0; i < response.length(); i++) {
                    JSONObject jsonObject = response.getJSONObject(i);
                    shoplistModel = new NotificationModel();
                    shoplistModel.setType(jsonObject.getString("type"));
                    AppDelegate.Log("notification", shoplistModel.getType() + "");
                    shoplistModel.setMessage(jsonObject.getString("message"));
                    productListModelArrayList.add(shoplistModel);
                    AppDelegate.Log("notification1", productListModelArrayList + "");
                }
                setProductList(productListModelArrayList);
                AppDelegate.Log("notification", productListModelArrayList + "");
            } else {
                AppDelegate.ShowDialog(getActivity(), obj.getString("message") + "", "Alert!!!");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setProductList(final ArrayList<NotificationModel> productListModelArrayList) {
        NotificationAdapter adapter = new NotificationAdapter(getActivity(), productListModelArrayList);
        AppDelegate.LogT("setProductList called");
        notification.setAdapter(adapter);
    }


}
