package com.noto.shorttrackhero;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import Constants.Tags;

public class EntryPoint extends FragmentActivity implements View.OnClickListener {
TextView Driver, Fan;
public  static  EntryPoint mActivity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.entry_point);
        mActivity=this;
         initializeObjects();
        getToken();
 }
    private void initializeObjects() {
        Driver=(TextView)findViewById(R.id.Driver);
        Driver.setOnClickListener(this);
        Fan=(TextView)findViewById(R.id.Fan);
        Fan.setOnClickListener(this);
    }
    private void getToken() {
        GCMClientManager pushClientManager = new GCMClientManager(this, getString(R.string.project_number));
        pushClientManager.registerIfNeeded(new GCMClientManager.RegistrationCompletedHandler() {
            @Override
            public void onSuccess(String registrationId, boolean isNewRegistration) {
                Log.d("Registration id", registrationId);

                AppDelegate.save(EntryPoint.this, registrationId, Tags.REGISTRATION_ID);

                //send this registrationId to your server
            }

            @Override
            public void onFailure(String ex) {
                super.onFailure(ex);
                Log.d("onFailure", ex);
                //   AppDelegate.save(EntryPoint.this, "estnJZDD5oE:APA91bH3-Wtz5h6YnfQJk1DNcPdPIJWGshhZKk0Ys5C4_KL-7i1UUCh9RDXmvafXsW3FTBNH501f97njVpVWiL2H-h6ZMwkA_mOxFT3iJoTNdmgNJ03W1uLNwa-0lynD_zNbzsYVyxdN", Tags.REGISTRATION_ID);

            }
        });
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mActivity!=null){
            mActivity=null;
        }
    }
    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.Driver:
                Intent i=new Intent(EntryPoint.this,Login.class);
                AppDelegate.save(this, Tags.IDENTIFY_DRIVER, Tags.IDENTIFIER);
                startActivity(i);
                break;
            case R.id.Fan:
                i=new Intent(EntryPoint.this,Login.class);
                AppDelegate.save(this, Tags.IDENTIFY_FAN, Tags.IDENTIFIER);
                startActivity(i);

                break;
        }
    }
    @Override
    public void onBackPressed() {
        finish();
        if(Splash.mActivity!=null){
            Splash.mActivity.finish();
        }
    }
}