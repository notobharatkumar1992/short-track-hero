package com.noto.shorttrackhero;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import adapters.TrackAdapter;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import model.LoginModel;
import model.PostAysnc_Model;
import model.TrackListModel;
import model.TrackModel;

import static com.noto.shorttrackhero.PlayVideo.saveBooleanInSP;
import static com.noto.shorttrackhero.PlayVideo.saveBooleanInSPDriver;

public class Select_Track extends AppCompatActivity implements OnReciveServerResponse, View.OnClickListener, OnDialogClickListener {
    private TrackListModel tracklistitem;
    ListView list;
    TextView save;
    private int track_id;
    private String track_name=".";
    private TrackModel trackModel;
    private LoginModel getProfile_model;
    int user_id;
    public static Activity mActivity;
    private String identifier;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select__track);
        mActivity = this;
        AdView adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .setRequestAgent("android_studio:ad_template").build();
        adView.loadAd(adRequest);
        identifier = AppDelegate.getValue(this, Tags.IDENTIFIER);
        list = (ListView) findViewById(R.id.select_track);
        save = (TextView) findViewById(R.id.save);
        execute_TRACKLIST();
        save.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        if (PlayVideo.activity != null) {
            PlayVideo.activity.finish();
        }
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mActivity != null) {
            mActivity = null;
        }
    }

    private void execute_TRACKLIST() {
        if (AppDelegate.haveNetworkConnection(this, false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
            PostAsync mPostAsyncObj;
            if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.DRIVER_TRACKLIST,
                        mPostArrayList, null);
            } else {
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.TRACKLIST,
                        mPostArrayList, null);
            }
            AppDelegate.showProgressDialog(this);
            mPostAsyncObj.execute();
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(this);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(this,"Service Time Out!!!", "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.TRACKLIST)) {
            AppDelegate.save(this, result, Parameters.savetracklist);
            parseTracklist();
        }
        if (apiName.equals(ServerRequestConstants.DRIVER_TRACKLIST)) {
            AppDelegate.save(this, result, Parameters.savetracklist);
            parseTracklist();
        } else if (apiName.equals(ServerRequestConstants.GET_DASHBOARD)) {
            AppDelegate.save(this, result, Parameters.get_dashboard);
            AppDelegate.Log("changeTrack", result);
            parse_changeTrack();
        } else if (apiName.equals(ServerRequestConstants.DRIVER_GET_DASHBOARD)) {
            AppDelegate.save(this, result, Parameters.get_dashboard);
            AppDelegate.Log("changeTrack", result);
            parse_changeTrack();
        }
    }
    private void parseTracklist() {
        String tracklist = AppDelegate.getValue(this, Parameters.savetracklist);
        try {
            JSONObject jsonObject = new JSONObject(tracklist);
            ArrayList<TrackListModel> trackListModels = new ArrayList<TrackListModel>();
            tracklistitem = new TrackListModel();
            tracklistitem.setStatus(jsonObject.getInt("status"));
            tracklistitem.setDataflow(jsonObject.getInt("dataFlow"));
            if (tracklistitem.getStatus() == 1) {
                JSONArray jsonArray = jsonObject.getJSONArray("response");
                for (int i = 0; i < jsonArray.length(); i++) {
                    tracklistitem = new TrackListModel();
                    JSONObject obj = jsonArray.getJSONObject(i);
                    tracklistitem.setId(obj.getInt("id"));
                    tracklistitem.setCompany_name(obj.getString("company_name"));
                    tracklistitem.setPayment_method_id(obj.getInt("payment_method_id"));
                    tracklistitem.setCreated(obj.getString("created"));
                    tracklistitem.setUser_id(obj.getInt("id"));
                    trackListModels.add(tracklistitem);
                }
                trackset(trackListModels);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }
    private void trackset(final ArrayList<TrackListModel> trackListModels) {
        TrackAdapter spinnerAdapter = new TrackAdapter(this, trackListModels);
        list.setAdapter(spinnerAdapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                track_name = trackListModels.get(position).getCompany_name();
                track_id = trackListModels.get(position).getId();
            }
        });
    }
    private void parseProfile() {
        String Register = AppDelegate.getValue(this, Parameters.saveLogin);
        AppDelegate.Log("response", Register + "");
        try {
            JSONObject jsonObject = new JSONObject(Register);
            int status = jsonObject.getInt("status");
            getProfile_model = new LoginModel();
            if (status == 1) {
                JSONObject response = jsonObject.getJSONObject("response");
                getProfile_model.setId(response.getInt("id"));
                user_id = getProfile_model.getId();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void changeTrack() {
        try {
            parseProfile();
            if (AppDelegate.haveNetworkConnection(this, false)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.user_id, user_id, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.track_id, track_id, ServerRequestConstants.Key_PostintValue);
                PostAsync mPostAsyncObj;
                if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                    mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.DRIVER_GET_DASHBOARD,
                            mPostArrayList, null);
                } else {
                    mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.GET_DASHBOARD,
                            mPostArrayList, null);
                }

                AppDelegate.showProgressDialog(this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(this, "Please try again.", "Alert!!!");
        }
    }
    private void parse_changeTrack() {
        String track_response = AppDelegate.getValue(this, Parameters.get_dashboard);
        AppDelegate.save(this, String.valueOf(track_id), Tags.TRACK_ID);
        AppDelegate.save(this, track_name, Tags.TRACK_NAME);
        Intent i = new Intent(Select_Track.this, HomeScreen.class);
        startActivity(i);
        finish();
       /* try {
            JSONObject object = new JSONObject(track_response);
            trackModel = new TrackModel();
            trackModel.setStatus(object.getInt("status"));
            trackModel.setDataflow(object.getInt("dataFlow"));
            if (trackModel.getStatus() == 1) {
//                JSONObject response = object.getJSONObject("response");
//                trackModel.setId(response.getInt("id"));
//                trackModel.setUser_id(response.getInt("user_id"));
//                trackModel.setTrack_id(response.getInt("track_id"));
//                trackModel.setTotal_credit(response.getInt("total_credit"));
//                trackModel.setTotal_point(response.getInt("total_point"));
//                trackModel.setMessage(object.getString("message"));
                AppDelegate.save(this, String.valueOf(track_id), Tags.TRACK_ID);
                AppDelegate.save(this, track_name, Tags.TRACK_NAME);
               *//* if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                    saveBooleanInSPDriver(this, true);
                } else {
                    saveBooleanInSP(this, true);
                }*//*
                Intent i = new Intent(Select_Track.this, HomeScreen.class);
                startActivity(i);
                finish();
            }
            else{
                AppDelegate.ShowDialog(this,"No track found.","Alert!!!");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }*/
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.save:
                if (track_name.equals(".")) {
                    AppDelegate.ShowDialog(this, "Please select Track.", "Alert !!!");
                } else {
                    AppDelegate.showDialog_twoButtons(Select_Track.this, "Are you sure? ", "saveTrack", this);
                }

                break;
        }
    }

    @Override
    public void setOnDialogClickListener(String name) {
        if(name.equals("saveTrack")){
            changeTrack();
            //AppDelegate.showToast(getActivity(),"Hellooo");
        }
    }
}
