package com.noto.shorttrackhero;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.VideoView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import model.PostAysnc_Model;

public class PlayVideo extends AppCompatActivity implements OnReciveServerResponse, OnDialogClickListener {

Boolean show=false;
    private VideoView videoView;
    public static Activity activity;
    private String identifier;
    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.play_video);
        identifier = AppDelegate.getValue(this, Tags.IDENTIFIER);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        findIDS();
        executevdo();
        activity = this;

    }

    private void executevdo() {
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                PostAsync mPostAsyncObj = new PostAsync(this, PlayVideo.this, ServerRequestConstants.DRIVER_VDO,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(this, "Please try again.", "Alert!!!");
        }
    }

    private void findIDS() {
        videoView = (VideoView) findViewById(R.id.video);
    }

    public static void saveBooleanInSP(Context _context, boolean value) {
        SharedPreferences preferences = _context.getSharedPreferences("PROJECTNAME", android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("ISLOGGEDIN", value);
        editor.commit();
    }
    public static void saveBooleanInSPDriver(Context _context, boolean value) {
        SharedPreferences preferences = _context.getSharedPreferences("PROJECTNAMED", android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("ISLOGGEDIN", value);
        editor.commit();
    }

    public static boolean getBooleanFromSP(Context _context) {
// TODO Auto-generated method stub
        SharedPreferences preferences = _context.getSharedPreferences("PROJECTNAME", android.content.Context.MODE_PRIVATE);
        return preferences.getBoolean("ISLOGGEDIN", false);
    }
    public static boolean getBooleanFromSPDriver(Context _context) {
// TODO Auto-generated method stub
        SharedPreferences preferences = _context.getSharedPreferences("PROJECTNAMED", android.content.Context.MODE_PRIVATE);
        return preferences.getBoolean("ISLOGGEDIN", false);
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (activity != null) {
            activity = null;
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(this);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(this, "Service Time Out!!!", "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.DRIVER_VDO)) {
            AppDelegate.save(this, result, Parameters.savevdo);
            parsevdo();
        }
    }
    private void parsevdo() {
        String vdo=AppDelegate.getValue(this,Parameters.savevdo);
        try {
            JSONObject obj=new JSONObject(vdo);
            int status=obj.getInt("status");
            if(status==1){
                 url=obj.getString("response");
                playvdo(url);
            }else{
                AppDelegate.showAlert(this,"Video not found");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void playvdo(String url) {
        final ProgressDialog pd=new ProgressDialog(this);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        pd.show();
      /*  MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);*/
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(show==false){
                    pd.dismiss();
                    AppDelegate.showDialog_okCancel(PlayVideo.this, "Service Time Out!!!", "next", "retry", PlayVideo.this);

                }
            }
    }, 60000);
         videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {

             @Override
             public boolean onError(MediaPlayer mp, int what, int extra) {
                 Log.d("video", "setOnErrorListener ");
                 pd.dismiss();
                 AppDelegate.ShowDialogID(PlayVideo.this, "Can't play this vedio", "Alert", "ERROR", PlayVideo.this);
                 return true;
             }
         });
         videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
             @Override
             public void onPrepared(MediaPlayer mp) {
                 show = true;
                 pd.dismiss();
                 mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                     @Override
                     public void onCompletion(MediaPlayer mp) {
                         Intent intent = new Intent(PlayVideo.this, Select_Track.class);
                         startActivity(intent);
                       /* if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                            if (getBooleanFromSPDriver(PlayVideo.this) == false) {
                                Intent intent = new Intent(PlayVideo.this, Select_Track.class);
                                startActivity(intent);
                            } else {
                                Intent intent = new Intent(PlayVideo.this, HomeScreen.class);
                                startActivity(intent);
                            }
                        } else {
                            if (getBooleanFromSP(PlayVideo.this) == false) {
                                Intent intent = new Intent(PlayVideo.this, Select_Track.class);
                                startActivity(intent);
                            } else {
                                Intent intent = new Intent(PlayVideo.this, HomeScreen.class);
                                startActivity(intent);
                            }
                        }*/

                     }
                 });
             }
         });
         // String str_path = "file:///android_asset/carracing.mp4";
         // videoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.carracing));
         videoView.setVideoURI(Uri.parse(url));
         videoView.requestFocus();
         videoView.start();
     }
     @Override
     public void setOnDialogClickListener(String name) {
         if (name.equalsIgnoreCase("ERROR")) {
             Intent intent = new Intent(PlayVideo.this, Select_Track.class);
             startActivity(intent);
         }else if (name.equalsIgnoreCase("next")) {
             Intent intent = new Intent(PlayVideo.this, Select_Track.class);
             startActivity(intent);
         } else if (name.equalsIgnoreCase("retry")) {
             playvdo(url);
         }

     }
 }
