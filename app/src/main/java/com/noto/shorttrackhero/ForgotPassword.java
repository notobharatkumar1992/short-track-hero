package com.noto.shorttrackhero;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import model.PostAysnc_Model;
import utils.ProgressD;

public class ForgotPassword extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse, OnDialogClickListener {
    private Intent intent;
    private String Email;
    EditText email_id;
    TextView submit;
    private boolean result;
    public static ForgotPassword forgotpassword;
    private Toolbar toolbar;
    private String identifier;
    private int status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password);
        forgotpassword = this;
        set_toolbar();
        initializeObjects();
    }
    private void set_toolbar() {
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    if(forgotpassword!=null)
                        forgotpassword.finish();
            }
        });
    }
    private void initializeObjects() {
        email_id = (EditText) findViewById(R.id.email_id);
        submit = (TextView) findViewById(R.id.submit);
        submit.setOnClickListener(this);
        identifier = AppDelegate.getValue(this, Tags.IDENTIFIER);
    }
    private boolean validate_emailid(String email) {
        boolean result = true;
        try {
            if (email == null) {
                result = false;
                AppDelegate.ShowDialog(ForgotPassword.this, "Please enter Email Id !!!", "Oops!!!");
            }
        } catch (Exception e) {
            intent = new Intent(ForgotPassword.this, Login.class);
            startActivity(intent);
        }
        if (!email.matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+")) {
            result = false;
            AppDelegate.ShowDialog(ForgotPassword.this, "Invalid Email Address", "Oops!!!");
        }
        return result;
    }
    @Override
    protected void onResume() {
        super.onResume();
        AppDelegate.hideKeyBoard(this);
    }
    @Override
    public void onBackPressed() {
       finish();
    }
    @Override
    protected void onDestroy() {
        if (forgotpassword != null) {
            forgotpassword = null;
        }
        ProgressD.hideProgress_dialog();
        System.gc();
        super.onDestroy();
    }
    private void ForgotPassword() {
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.email, Email);
                PostAsync mPostAsyncObj;
                if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                    mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.FORGOT_PASS_DRIVER,
                            mPostArrayList, null);
                } else {
                     mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.FORGOT_PASSWORD,
                            mPostArrayList, null);
                }

                AppDelegate.showProgressDialog(this);
                mPostAsyncObj.execute();
            }
        }catch (Exception e){
            AppDelegate.ShowDialog(this, "Please try again.", "Alert!!!");
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.submit:
                Email = email_id.getText().toString();
                result = validate_emailid(Email);
                if (result == true) {
                    ForgotPassword();
                }
          }
    }
    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(this);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(this,"Service Time Out!!!","Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.FORGOT_PASSWORD)) {
            AppDelegate.save(this, result, Parameters.saveForgot);
            parseForgot();
        }
        if (apiName.equals(ServerRequestConstants.FORGOT_PASS_DRIVER)) {
            AppDelegate.save(this, result, Parameters.saveForgot);
            parseForgot();
        }
    }
    private void parseForgot() {
        String response=AppDelegate.getValue(this,Parameters.saveForgot);
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response);
             status = jsonObject.getInt("status");
            String message = jsonObject.getString("message");
           // AppDelegate.ShowDialog(ForgotPassword.this, message + "", "Alert!!!");
            AppDelegate.ShowDialogID(ForgotPassword.this, message + "", "Alert!!!", "ForgotPassword", this);
            if (status == 1) {
             //   finish();
            } else {
               // email_id.setText("");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setOnDialogClickListener(String name) {
        if(name.equals("ForgotPassword")){
            if (status == 1) {
                  finish();
            } else {
                email_id.setText("");
            }
        }
    }
}
