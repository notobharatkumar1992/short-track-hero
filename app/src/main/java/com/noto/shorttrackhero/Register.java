package com.noto.shorttrackhero;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;

import Constants.Tags;
import fragments.DriverRegister;
import fragments.FanRegister;

public class Register extends AppCompatActivity {
    String identifier;
    Toolbar toolbar;
    FrameLayout frame;
   public static Register activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver);
        activity = this;
        initializeObjects();
        settoolbar();
        identifier = AppDelegate.getValue(this, Tags.IDENTIFIER);
        AppDelegate.Log("identifier", identifier + "");
        showfragment();
    }
    private void showfragment() {
        frame.setBackgroundColor(Color.WHITE);
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
            AppDelegate.showFragmentAnimation(fragmentManager, new DriverRegister(), R.id.frame);
        } else {
            AppDelegate.showFragmentAnimation(fragmentManager, new FanRegister(), R.id.frame);
        }
    }
    private void initializeObjects() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        frame = (FrameLayout) findViewById(R.id.frame);
    }
    private void settoolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        //  toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getSupportFragmentManager().findFragmentById(R.id.frame) instanceof DriverRegister) {
                    if (activity != null)
                        activity.finish();
                }
                if (getSupportFragmentManager().findFragmentById(R.id.frame) instanceof FanRegister) {
                    if (activity != null)
                        activity.finish();
                }
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().findFragmentById(R.id.frame) instanceof DriverRegister) {
            if (activity != null)
                activity.finish();
        }
        if (getSupportFragmentManager().findFragmentById(R.id.frame) instanceof FanRegister) {
            if (activity != null)
                activity.finish();
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (activity != null) {
            activity = null;
        }
    }
}
