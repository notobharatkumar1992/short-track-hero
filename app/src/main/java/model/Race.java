package model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by admin on 29-06-2016.
 */
public class Race implements Parcelable
{

    protected Race(Parcel in) {
        ranklist = in.createTypedArrayList(RankList.CREATOR);
        race_id = in.readInt();
        name = in.readString();
        addresss = in.readString();
        first_winner = in.readString();
        second_winner = in.readString();
        third_winner = in.readString();
        first_runner_up = in.readString();
        second_runner_up = in.readString();
        lap = in.readString();
        other = in.readString();
        start_date = in.readString();
        end_date = in.readString();
        total_point=in.readInt();
    }

    public static final Creator<Race> CREATOR = new Creator<Race>() {
        @Override
        public Race createFromParcel(Parcel in) {
            return new Race(in);
        }

        @Override
        public Race[] newArray(int size) {
            return new Race[size];
        }
    };

    public ArrayList<RankList> getRanklist() {
        return ranklist;
    }

    public void setRanklist(ArrayList<RankList> ranklist) {
        this.ranklist = ranklist;
    }

    ArrayList<RankList> ranklist;
    int race_id,total_point;

    public int getTotal_point() {
        return total_point;
    }

    public void setTotal_point(int total_point) {
        this.total_point = total_point;
    }

    String name, addresss, first_winner, second_winner, third_winner,first_runner_up,second_runner_up,lap,other,start_date,end_date;


    public Race() {

    }


    public int getRace_id() {
        return race_id;
    }

    public void setRace_id(int race_id) {
        this.race_id = race_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddresss() {
        return addresss;
    }

    public void setAddresss(String addresss) {
        this.addresss = addresss;
    }

    public String getFirst_winner() {
        return first_winner;
    }

    public void setFirst_winner(String first_winner) {
        this.first_winner = first_winner;
    }

    public String getSecond_winner() {
        return second_winner;
    }

    public void setSecond_winner(String second_winner) {
        this.second_winner = second_winner;
    }

    public String getThird_winner() {
        return third_winner;
    }

    public void setThird_winner(String third_winner) {
        this.third_winner = third_winner;
    }

    public String getFirst_runner_up() {
        return first_runner_up;
    }

    public void setFirst_runner_up(String first_runner_up) {
        this.first_runner_up = first_runner_up;
    }

    public String getSecond_runner_up() {
        return second_runner_up;
    }

    public void setSecond_runner_up(String second_runner_up) {
        this.second_runner_up = second_runner_up;
    }

    public String getLap() {
        return lap;
    }

    public void setLap(String lap) {
        this.lap = lap;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(ranklist);
        dest.writeInt(race_id);
        dest.writeString(name);
        dest.writeString(addresss);
        dest.writeString(first_winner);
        dest.writeString(second_winner);
        dest.writeString(third_winner);
        dest.writeString(first_runner_up);
        dest.writeString(second_runner_up);
        dest.writeString(lap);
        dest.writeString(other);
        dest.writeString(start_date);
        dest.writeString(end_date);
        dest.writeInt(total_point);
    }

    @Override
    public String toString() {
        return "Race{" +
                "ranklist=" + ranklist +
                ", race_id=" + race_id +
                ", total_point=" + total_point +
                ", name='" + name + '\'' +
                ", addresss='" + addresss + '\'' +
                ", first_winner='" + first_winner + '\'' +
                ", second_winner='" + second_winner + '\'' +
                ", third_winner='" + third_winner + '\'' +
                ", first_runner_up='" + first_runner_up + '\'' +
                ", second_runner_up='" + second_runner_up + '\'' +
                ", lap='" + lap + '\'' +
                ", other='" + other + '\'' +
                ", start_date='" + start_date + '\'' +
                ", end_date='" + end_date + '\'' +
                '}';
    }
}
