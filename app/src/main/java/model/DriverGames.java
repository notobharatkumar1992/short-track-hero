package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 01-07-2016.
 */
public class DriverGames implements Parcelable{
    String competition_name,start_date,lap;
    int total_credit,total_point;

    public DriverGames() {
    }

    @Override
    public String toString() {
        return "DriverGames{" +
                "competition_name='" + competition_name + '\'' +
                ", start_date='" + start_date + '\'' +
                ", lap='" + lap + '\'' +
                ", total_credit=" + total_credit +
                ", total_point=" + total_point +
                '}';
    }

    protected DriverGames(Parcel in) {
        competition_name = in.readString();
        start_date = in.readString();
        lap = in.readString();
        total_credit = in.readInt();
        total_point = in.readInt();
    }

    public static final Creator<DriverGames> CREATOR = new Creator<DriverGames>() {
        @Override
        public DriverGames createFromParcel(Parcel in) {
            return new DriverGames(in);
        }

        @Override
        public DriverGames[] newArray(int size) {
            return new DriverGames[size];
        }
    };

    public String getCompetition_name() {
        return competition_name;
    }

    public void setCompetition_name(String competition_name) {
        this.competition_name = competition_name;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getLap() {
        return lap;
    }

    public void setLap(String lap) {
        this.lap = lap;
    }

    public int getTotal_credit() {
        return total_credit;
    }

    public void setTotal_credit(int total_credit) {
        this.total_credit = total_credit;
    }

    public int getTotal_point() {
        return total_point;
    }

    public void setTotal_point(int total_point) {
        this.total_point = total_point;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(competition_name);
        dest.writeString(start_date);
        dest.writeString(lap);
        dest.writeInt(total_credit);
        dest.writeInt(total_point);
    }
}
