package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 15-07-2016.
 */
public class GameHistoryModel implements Parcelable{
    int status,dataflow,id,track_name,position,total_credit,lap,total_point;
    String driver_name,fan_name,competitions_name,race_name,date,time,address,current_rank,image,message;

    public GameHistoryModel() {
    }

    @Override
    public String toString() {
        return "GameHistoryModel{" +
                "status=" + status +
                ", dataflow=" + dataflow +
                ", id=" + id +
                ", track_name=" + track_name +
                ", position=" + position +
                ", total_credit=" + total_credit +
                ", lap=" + lap +
                ", total_point=" + total_point +
                ", driver_name='" + driver_name + '\'' +
                ", fan_name='" + fan_name + '\'' +
                ", competitions_name='" + competitions_name + '\'' +
                ", race_name='" + race_name + '\'' +
                ", date='" + date + '\'' +
                ", time='" + time + '\'' +
                ", address='" + address + '\'' +
                ", current_rank='" + current_rank + '\'' +
                ", image='" + image + '\'' +
                ", message='" + message + '\'' +
                '}';
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getDataflow() {
        return dataflow;
    }

    public void setDataflow(int dataflow) {
        this.dataflow = dataflow;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTrack_name() {
        return track_name;
    }

    public void setTrack_name(int track_name) {
        this.track_name = track_name;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getTotal_credit() {
        return total_credit;
    }

    public void setTotal_credit(int total_credit) {
        this.total_credit = total_credit;
    }

    public int getLap() {
        return lap;
    }

    public void setLap(int lap) {
        this.lap = lap;
    }

    public int getTotal_point() {
        return total_point;
    }

    public void setTotal_point(int total_point) {
        this.total_point = total_point;
    }

    public String getDriver_name() {
        return driver_name;
    }

    public void setDriver_name(String driver_name) {
        this.driver_name = driver_name;
    }

    public String getFan_name() {
        return fan_name;
    }

    public void setFan_name(String fan_name) {
        this.fan_name = fan_name;
    }

    public String getCompetitions_name() {
        return competitions_name;
    }

    public void setCompetitions_name(String competitions_name) {
        this.competitions_name = competitions_name;
    }

    public String getRace_name() {
        return race_name;
    }

    public void setRace_name(String race_name) {
        this.race_name = race_name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCurrent_rank() {
        return current_rank;
    }

    public void setCurrent_rank(String current_rank) {
        this.current_rank = current_rank;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public GameHistoryModel(Parcel in) {
        status = in.readInt();
        dataflow = in.readInt();
        id = in.readInt();
        track_name = in.readInt();
        position = in.readInt();
        total_credit = in.readInt();
        lap = in.readInt();
        total_point = in.readInt();
        driver_name = in.readString();
        fan_name = in.readString();
        competitions_name = in.readString();
        race_name = in.readString();
        date = in.readString();
        time = in.readString();
        address = in.readString();
        current_rank = in.readString();
        image = in.readString();
        message = in.readString();
    }

    public static final Creator<GameHistoryModel> CREATOR = new Creator<GameHistoryModel>() {
        @Override
        public GameHistoryModel createFromParcel(Parcel in) {
            return new GameHistoryModel(in);
        }

        @Override
        public GameHistoryModel[] newArray(int size) {
            return new GameHistoryModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeInt(dataflow);
        dest.writeInt(id);
        dest.writeInt(track_name);
        dest.writeInt(position);
        dest.writeInt(total_credit);
        dest.writeInt(lap);
        dest.writeInt(total_point);
        dest.writeString(driver_name);
        dest.writeString(fan_name);
        dest.writeString(competitions_name);
        dest.writeString(race_name);
        dest.writeString(date);
        dest.writeString(time);
        dest.writeString(address);
        dest.writeString(current_rank);
        dest.writeString(image);
        dest.writeString(message);
    }
}
