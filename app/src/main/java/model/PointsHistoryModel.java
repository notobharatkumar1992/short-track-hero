package model;

import android.os.Parcel;
import android.os.Parcelable;
/**
 * Created by admin on 05-07-2016.
 */
public class PointsHistoryModel implements Parcelable {
   int status,id,dataFlow,position,total_credit,total_point;
    String driver_name,fan_name,track_name,competitions_name,race_name,date,time,lap,address,current_rank,image;

    protected PointsHistoryModel(Parcel in) {
        status = in.readInt();
        id = in.readInt();
        dataFlow = in.readInt();
        position = in.readInt();
        total_credit = in.readInt();
        total_point = in.readInt();
        driver_name = in.readString();
        fan_name = in.readString();
        track_name = in.readString();
        competitions_name = in.readString();
        race_name = in.readString();
        date = in.readString();
        time = in.readString();
        lap = in.readString();
        address = in.readString();
        current_rank = in.readString();
        image = in.readString();
    }

    public static final Creator<PointsHistoryModel> CREATOR = new Creator<PointsHistoryModel>() {
        @Override
        public PointsHistoryModel createFromParcel(Parcel in) {
            return new PointsHistoryModel(in);
        }

        @Override
        public PointsHistoryModel[] newArray(int size) {
            return new PointsHistoryModel[size];
        }
    };

    @Override
    public String toString() {
        return "PointsHistoryModel{" +
                "status=" + status +
                ", id=" + id +
                ", dataFlow=" + dataFlow +
                ", position=" + position +
                ", total_credit=" + total_credit +
                ", total_point=" + total_point +
                ", driver_name='" + driver_name + '\'' +
                ", fan_name='" + fan_name + '\'' +
                ", track_name='" + track_name + '\'' +
                ", competitions_name='" + competitions_name + '\'' +
                ", race_name='" + race_name + '\'' +
                ", date='" + date + '\'' +
                ", time='" + time + '\'' +
                ", lap='" + lap + '\'' +
                ", address='" + address + '\'' +
                ", current_rank='" + current_rank + '\'' +
                ", image='" + image + '\'' +
                '}';
    }

    public PointsHistoryModel() {
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDataFlow() {
        return dataFlow;
    }

    public void setDataFlow(int dataFlow) {
        this.dataFlow = dataFlow;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getTotal_credit() {
        return total_credit;
    }

    public void setTotal_credit(int total_credit) {
        this.total_credit = total_credit;
    }

    public int getTotal_point() {
        return total_point;
    }

    public void setTotal_point(int total_point) {
        this.total_point = total_point;
    }

    public String getDriver_name() {
        return driver_name;
    }

    public void setDriver_name(String driver_name) {
        this.driver_name = driver_name;
    }

    public String getFan_name() {
        return fan_name;
    }

    public void setFan_name(String fan_name) {
        this.fan_name = fan_name;
    }

    public String getTrack_name() {
        return track_name;
    }

    public void setTrack_name(String track_name) {
        this.track_name = track_name;
    }

    public String getCompetitions_name() {
        return competitions_name;
    }

    public void setCompetitions_name(String competitions_name) {
        this.competitions_name = competitions_name;
    }

    public String getRace_name() {
        return race_name;
    }

    public void setRace_name(String race_name) {
        this.race_name = race_name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getLap() {
        return lap;
    }

    public void setLap(String lap) {
        this.lap = lap;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCurrent_rank() {
        return current_rank;
    }

    public void setCurrent_rank(String current_rank) {
        this.current_rank = current_rank;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeInt(id);
        dest.writeInt(dataFlow);
        dest.writeInt(position);
        dest.writeInt(total_credit);
        dest.writeInt(total_point);
        dest.writeString(driver_name);
        dest.writeString(fan_name);
        dest.writeString(track_name);
        dest.writeString(competitions_name);
        dest.writeString(race_name);
        dest.writeString(date);
        dest.writeString(time);
        dest.writeString(lap);
        dest.writeString(address);
        dest.writeString(current_rank);
        dest.writeString(image);
    }
}
