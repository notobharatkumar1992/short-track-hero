package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 29-06-2016.
 */
public class TrackModel implements Parcelable{
    int status, dataflow,id,user_id,track_id,total_credit,total_point;
    String message;

    public TrackModel() {
    }

    @Override
    public String toString() {
        return "TrackModel{" +
                "status=" + status +
                ", dataflow=" + dataflow +
                ", id=" + id +
                ", user_id=" + user_id +
                ", track_id=" + track_id +
                ", total_credit=" + total_credit +
                ", total_point=" + total_point +
                ", message='" + message + '\'' +
                '}';
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getDataflow() {
        return dataflow;
    }

    public void setDataflow(int dataflow) {
        this.dataflow = dataflow;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getTrack_id() {
        return track_id;
    }

    public void setTrack_id(int track_id) {
        this.track_id = track_id;
    }

    public int getTotal_credit() {
        return total_credit;
    }

    public void setTotal_credit(int total_credit) {
        this.total_credit = total_credit;
    }

    public int getTotal_point() {
        return total_point;
    }

    public void setTotal_point(int total_point) {
        this.total_point = total_point;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static Creator<TrackModel> getCREATOR() {
        return CREATOR;
    }

    protected TrackModel(Parcel in) {
        status = in.readInt();
        dataflow = in.readInt();
        id = in.readInt();
        user_id = in.readInt();
        track_id = in.readInt();
        total_credit = in.readInt();
        total_point = in.readInt();
        message = in.readString();
    }

    public static final Creator<TrackModel> CREATOR = new Creator<TrackModel>() {
        @Override
        public TrackModel createFromParcel(Parcel in) {
            return new TrackModel(in);
        }

        @Override
        public TrackModel[] newArray(int size) {
            return new TrackModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeInt(dataflow);
        dest.writeInt(id);
        dest.writeInt(user_id);
        dest.writeInt(track_id);
        dest.writeInt(total_credit);
        dest.writeInt(total_point);
        dest.writeString(message);
    }
}
