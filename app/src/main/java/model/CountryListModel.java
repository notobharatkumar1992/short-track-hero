package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 17-06-2016.
 */
public class CountryListModel implements Parcelable{
   int status, dataflow,id;
    String name,code;

    public CountryListModel() {
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getDataflow() {
        return dataflow;
    }

    public void setDataflow(int dataflow) {
        this.dataflow = dataflow;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static Creator<CountryListModel> getCREATOR() {
        return CREATOR;
    }

    protected CountryListModel(Parcel in) {
        status = in.readInt();
        dataflow = in.readInt();
        id = in.readInt();
        name = in.readString();
        code = in.readString();
    }

    public static final Creator<CountryListModel> CREATOR = new Creator<CountryListModel>() {
        @Override
        public CountryListModel createFromParcel(Parcel in) {
            return new CountryListModel(in);
        }

        @Override
        public CountryListModel[] newArray(int size) {
            return new CountryListModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeInt(dataflow);
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(code);
    }
}
