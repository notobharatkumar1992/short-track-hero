package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 27-07-2016.
 */
public class RankList implements Parcelable {
    String rank,point;

    @Override
    public String toString() {
        return "RankList{" +
                "rank='" + rank + '\'' +
                ", point='" + point + '\'' +
                '}';
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public RankList() {
    }

    protected RankList(Parcel in) {
        rank = in.readString();
        point = in.readString();
    }

    public static final Creator<RankList> CREATOR = new Creator<RankList>() {
        @Override
        public RankList createFromParcel(Parcel in) {
            return new RankList(in);
        }

        @Override
        public RankList[] newArray(int size) {
            return new RankList[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(rank);
        dest.writeString(point);
    }
}
