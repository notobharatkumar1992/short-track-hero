package model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by admin on 29-06-2016.
 */
public class SponcerBannerModel implements Parcelable{
    ArrayList<SponcerModel> arrayListHashMap;

    public SponcerBannerModel() {
    }
    @Override
    public String toString() {
        return "SponcerBannerModel{" +
                "arrayListHashMap=" + arrayListHashMap +
                '}';
    }
    public SponcerBannerModel( ArrayList<SponcerModel> arrayListHashMap) {
        this.arrayListHashMap = arrayListHashMap;
    }
    protected SponcerBannerModel(Parcel in) {
    }
    public static final Creator<SponcerBannerModel> CREATOR = new Creator<SponcerBannerModel>() {
        @Override
        public SponcerBannerModel createFromParcel(Parcel in) {
            return new SponcerBannerModel(in);
        }
        @Override
        public SponcerBannerModel[] newArray(int size) {
            return new SponcerBannerModel[size];
        }
    };
    public  ArrayList<SponcerModel> getArrayListHashMap() {
        return arrayListHashMap;
    }
    @Override
    public int describeContents() {
        return 0;
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}
