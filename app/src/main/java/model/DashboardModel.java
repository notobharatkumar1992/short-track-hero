package model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by admin on 28-06-2016.
 */
public class DashboardModel implements Parcelable {
    int status, dataflow, id;
    String Competition_name;
    ArrayList<RaceModel> races = new ArrayList<>();

    public ArrayList<RaceModel> getRaces() {
        return races;
    }

    @Override
    public String toString() {
        return "DashboardModel{" +
                "status=" + status +
                ", dataflow=" + dataflow +
                ", id=" + id +
                ", Competition_name='" + Competition_name + '\'' +
                ", races=" + races +
                '}';
    }

    public void setRaces(ArrayList<RaceModel> races) {
        this.races = races;
    }

    public static Creator<DashboardModel> getCREATOR() {
        return CREATOR;
    }

    protected DashboardModel(Parcel in) {
        status = in.readInt();
        dataflow = in.readInt();
        id = in.readInt();


        Competition_name = in.readString();


    }

    public DashboardModel() {
    }

    public static final Creator<DashboardModel> CREATOR = new Creator<DashboardModel>() {
        @Override
        public DashboardModel createFromParcel(Parcel in) {
            return new DashboardModel(in);
        }

        @Override
        public DashboardModel[] newArray(int size) {
            return new DashboardModel[size];
        }
    };


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getDataflow() {
        return dataflow;
    }

    public void setDataflow(int dataflow) {
        this.dataflow = dataflow;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getCompetition_name() {
        return Competition_name;
    }

    public void setCompetition_name(String competition_name) {
        Competition_name = competition_name;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeInt(dataflow);
        dest.writeInt(id);
        dest.writeString(Competition_name);
    }
}
