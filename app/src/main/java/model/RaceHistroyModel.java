package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 14-07-2016.
 */
public class RaceHistroyModel implements Parcelable{
    int status,dataflow,fan_id,track_id,competition_id,race_id;
    String track_name,competition_name,race_name;

    public RaceHistroyModel() {
    }

    @Override
    public String toString() {
        return "RaceHistroyModel{" +
                "status=" + status +
                ", dataflow=" + dataflow +
                ", fan_id=" + fan_id +
                ", track_id=" + track_id +
                ", competition_id=" + competition_id +
                ", race_id=" + race_id +
                ", track_name='" + track_name + '\'' +
                ", competition_name='" + competition_name + '\'' +
                ", race_name='" + race_name + '\'' +
                '}';
    }

    public String getRace_name() {
        return race_name;
    }

    public void setRace_name(String race_name) {
        this.race_name = race_name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getDataflow() {
        return dataflow;
    }

    public void setDataflow(int dataflow) {
        this.dataflow = dataflow;
    }

    public int getFan_id() {
        return fan_id;
    }

    public void setFan_id(int fan_id) {
        this.fan_id = fan_id;
    }

    public int getTrack_id() {
        return track_id;
    }

    public void setTrack_id(int track_id) {
        this.track_id = track_id;
    }

    public int getCompetition_id() {
        return competition_id;
    }

    public void setCompetition_id(int competition_id) {
        this.competition_id = competition_id;
    }

    public int getRace_id() {
        return race_id;
    }

    public void setRace_id(int race_id) {
        this.race_id = race_id;
    }

    public String getTrack_name() {
        return track_name;
    }

    public void setTrack_name(String track_name) {
        this.track_name = track_name;
    }

    public String getCompetition_name() {
        return competition_name;
    }

    public void setCompetition_name(String competition_name) {
        this.competition_name = competition_name;
    }

    protected RaceHistroyModel(Parcel in) {
        status = in.readInt();
        dataflow = in.readInt();
        fan_id = in.readInt();
        track_id = in.readInt();
        competition_id = in.readInt();
        race_id = in.readInt();
        track_name = in.readString();
        competition_name = in.readString();
        race_name = in.readString();
    }

    public static final Creator<RaceHistroyModel> CREATOR = new Creator<RaceHistroyModel>() {
        @Override
        public RaceHistroyModel createFromParcel(Parcel in) {
            return new RaceHistroyModel(in);
        }

        @Override
        public RaceHistroyModel[] newArray(int size) {
            return new RaceHistroyModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeInt(dataflow);
        dest.writeInt(fan_id);
        dest.writeInt(track_id);
        dest.writeInt(competition_id);
        dest.writeInt(race_id);
        dest.writeString(track_name);
        dest.writeString(competition_name);
        dest.writeString(race_name);
    }
}
