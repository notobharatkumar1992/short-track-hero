package model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by admin on 01-07-2016.
 */
public class DriverDetailModel implements Parcelable {
    int status,dataflow;
    Driver driver;
    ArrayList<DriverGames> driverGames=new ArrayList<>();
    ArrayList<SponcerModel> sponcer=new ArrayList<>();

    protected DriverDetailModel(Parcel in) {
        status = in.readInt();
        dataflow = in.readInt();
        driver = in.readParcelable(Driver.class.getClassLoader());
        driverGames = in.createTypedArrayList(DriverGames.CREATOR);
        sponcer = in.createTypedArrayList(SponcerModel.CREATOR);
    }

    public static final Creator<DriverDetailModel> CREATOR = new Creator<DriverDetailModel>() {
        @Override
        public DriverDetailModel createFromParcel(Parcel in) {
            return new DriverDetailModel(in);
        }
        @Override
        public DriverDetailModel[] newArray(int size) {
            return new DriverDetailModel[size];
        }
    };
    @Override
    public String toString() {
        return "DriverDetailModel{" +
                "status=" + status +
                ", dataflow=" + dataflow +
                ", driver=" + driver +
                ", driverGames=" + driverGames +
                ", sponcer=" + sponcer +
                '}';
    }

    public DriverDetailModel() {
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getDataflow() {
        return dataflow;
    }

    public void setDataflow(int dataflow) {
        this.dataflow = dataflow;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public ArrayList<DriverGames> getDriverGames() {
        return driverGames;
    }

    public void setDriverGames(ArrayList<DriverGames> driverGames) {
        this.driverGames = driverGames;
    }

    public ArrayList<SponcerModel> getSponcer() {
        return sponcer;
    }

    public void setSponcer(ArrayList<SponcerModel> sponcer) {
        this.sponcer = sponcer;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeInt(dataflow);
        dest.writeParcelable(driver, flags);
        dest.writeTypedList(driverGames);
        dest.writeTypedList(sponcer);
    }
}
