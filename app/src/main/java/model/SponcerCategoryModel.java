package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 13-07-2016.
 */
public class SponcerCategoryModel implements Parcelable {
int status,dataflow,category_id;
    String name;

    public int getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "SponcerCategoryModel{" +
                "status=" + status +
                ", dataflow=" + dataflow +
                ", category_id=" + category_id +
                ", name='" + name + '\'' +
                '}';
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getDataflow() {
        return dataflow;
    }

    public void setDataflow(int dataflow) {
        this.dataflow = dataflow;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SponcerCategoryModel() {
    }

    protected SponcerCategoryModel(Parcel in) {
        status = in.readInt();
        dataflow = in.readInt();
        category_id = in.readInt();
        name = in.readString();
    }

    public static final Creator<SponcerCategoryModel> CREATOR = new Creator<SponcerCategoryModel>() {
        @Override
        public SponcerCategoryModel createFromParcel(Parcel in) {
            return new SponcerCategoryModel(in);
        }

        @Override
        public SponcerCategoryModel[] newArray(int size) {
            return new SponcerCategoryModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeInt(dataflow);
        dest.writeInt(category_id);
        dest.writeString(name);
    }
}
