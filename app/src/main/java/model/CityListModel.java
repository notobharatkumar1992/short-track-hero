package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 17-06-2016.
 */
public class CityListModel implements Parcelable{
int status, dataflow,id;
    String name;

    public CityListModel() {
    }

    @Override
    public String toString() {
        return "CityListModel{" +
                "status=" + status +
                ", dataflow=" + dataflow +
                ", id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public static Creator<CityListModel> getCREATOR() {
        return CREATOR;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getDataflow() {
        return dataflow;
    }

    public void setDataflow(int dataflow) {
        this.dataflow = dataflow;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    protected CityListModel(Parcel in) {
        status = in.readInt();
        dataflow = in.readInt();
        id = in.readInt();
        name = in.readString();
    }

    public static final Creator<CityListModel> CREATOR = new Creator<CityListModel>() {
        @Override
        public CityListModel createFromParcel(Parcel in) {
            return new CityListModel(in);
        }

        @Override
        public CityListModel[] newArray(int size) {
            return new CityListModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeInt(dataflow);
        dest.writeInt(id);
        dest.writeString(name);
    }
}
