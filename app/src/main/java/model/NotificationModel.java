package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 14-07-2016.
 */
public class NotificationModel implements Parcelable{
    int status,dataflow;
    String type,message;

    @Override
    public String toString() {
        return "NotificationModel{" +
                "status=" + status +
                ", dataflow=" + dataflow +
                ", type='" + type + '\'' +
                ", message='" + message + '\'' +
                '}';
    }

    public NotificationModel() {
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getDataflow() {
        return dataflow;
    }

    public void setDataflow(int dataflow) {
        this.dataflow = dataflow;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    protected NotificationModel(Parcel in) {
        status = in.readInt();
        dataflow = in.readInt();
        type = in.readString();
        message = in.readString();
    }

    public static final Creator<NotificationModel> CREATOR = new Creator<NotificationModel>() {
        @Override
        public NotificationModel createFromParcel(Parcel in) {
            return new NotificationModel(in);
        }

        @Override
        public NotificationModel[] newArray(int size) {
            return new NotificationModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeInt(dataflow);
        dest.writeString(type);
        dest.writeString(message);
    }
}
