package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 17-06-2016.
 */
public class StateListModel implements Parcelable{
    int status, dataflow, id;
    String region_name;

    public StateListModel() {
    }

    protected StateListModel(Parcel in) {
        status = in.readInt();
        dataflow = in.readInt();
        id = in.readInt();
        region_name = in.readString();
    }

    public int getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "StateListModel{" +
                "status=" + status +
                ", dataflow=" + dataflow +
                ", id=" + id +
                ", region_name='" + region_name + '\'' +
                '}';
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getDataflow() {
        return dataflow;
    }

    public void setDataflow(int dataflow) {
        this.dataflow = dataflow;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRegion_name() {
        return region_name;
    }

    public void setRegion_name(String region_name) {
        this.region_name = region_name;
    }

    public static Creator<StateListModel> getCREATOR() {
        return CREATOR;
    }

    public static final Creator<StateListModel> CREATOR = new Creator<StateListModel>() {
        @Override
        public StateListModel createFromParcel(Parcel in) {
            return new StateListModel(in);
        }

        @Override
        public StateListModel[] newArray(int size) {
            return new StateListModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeInt(dataflow);
        dest.writeInt(id);
        dest.writeString(region_name);
    }
}
