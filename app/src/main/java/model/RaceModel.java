package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 28-06-2016.
 */
public class RaceModel implements Parcelable {
    int competition_id,race_id;
    String class_race_name;

    protected RaceModel(Parcel in) {
        competition_id = in.readInt();
        race_id = in.readInt();
        class_race_name = in.readString();
    }

    public static final Creator<RaceModel> CREATOR = new Creator<RaceModel>() {
        @Override
        public RaceModel createFromParcel(Parcel in) {
            return new RaceModel(in);
        }

        @Override
        public RaceModel[] newArray(int size) {
            return new RaceModel[size];
        }
    };

    @Override
    public String toString() {
        return "RaceModel{" +
                "competition_id=" + competition_id +
                ", id=" + race_id +
                ", class_race_name='" + class_race_name + '\'' +
                '}';
    }

    public RaceModel() {
    }

    public int getCompetition_id() {
        return competition_id;
    }

    public void setCompetition_id(int competition_id) {
        this.competition_id = competition_id;
    }

    public int getRace_id() {
        return race_id;
    }

    public void setRace_id(int id) {
        this.race_id = id;
    }

    public String getClass_race_name() {
        return class_race_name;
    }

    public void setClass_race_name(String class_race_name) {
        this.class_race_name = class_race_name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(competition_id);
        dest.writeInt(race_id);
        dest.writeString(class_race_name);
    }
}
