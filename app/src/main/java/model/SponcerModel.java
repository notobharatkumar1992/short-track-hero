package model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by admin on 28-06-2016.
 */
public class SponcerModel implements Parcelable {
    int sponcer_id;
    String sponcer_name,sponcer_img,thump_img;

    public SponcerModel(Parcel in) {
        sponcer_id = in.readInt();
        sponcer_name = in.readString();
        sponcer_img = in.readString();
        thump_img = in.readString();
    }

    @Override
    public String toString() {
        return "SponcerModel{" +
                "sponcer_id=" + sponcer_id +
                ", sponcer_name='" + sponcer_name + '\'' +
                ", sponcer_img='" + sponcer_img + '\'' +
                ", thump_img='" + thump_img + '\'' +
                '}';
    }

    public static final Creator<SponcerModel> CREATOR = new Creator<SponcerModel>() {
        @Override
        public SponcerModel createFromParcel(Parcel in) {
            return new SponcerModel(in);
        }

        @Override
        public SponcerModel[] newArray(int size) {
            return new SponcerModel[size];
        }
    };

    public SponcerModel(ArrayList<SponcerModel> sponcerModels) {
    }

    public int getSponcer_id() {
        return sponcer_id;
    }

    public void setSponcer_id(int sponcer_id) {
        this.sponcer_id = sponcer_id;
    }

    public String getSponcer_name() {
        return sponcer_name;
    }

    public void setSponcer_name(String sponcer_name) {
        this.sponcer_name = sponcer_name;
    }

    public String getSponcer_img() {
        return sponcer_img;
    }

    public void setSponcer_img(String sponcer_img) {
        this.sponcer_img = sponcer_img;
    }

    public String getThump_img() {
        return thump_img;
    }

    public void setThump_img(String thump_img) {
        this.thump_img = thump_img;
    }

    public SponcerModel() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(sponcer_id);
        dest.writeString(sponcer_name);
        dest.writeString(sponcer_img);
        dest.writeString(thump_img);
    }
}
