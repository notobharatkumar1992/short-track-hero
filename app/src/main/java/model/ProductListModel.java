package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 04-07-2016.
 */
public class ProductListModel implements Parcelable {
    int status, dataflow, product_id, quantity;
    String product_name, image, description,quantityString;
    String image1;
    String image2;
    String fan_name;
    double  price;

    @Override
    public String toString() {
        return "ProductListModel{" +
                "status=" + status +
                ", dataflow=" + dataflow +
                ", product_id=" + product_id +
                ", price=" + price +
                ", quantity=" + quantity +
                ", product_name='" + product_name + '\'' +
                ", image='" + image + '\'' +
                ", description='" + description + '\'' +
                ", quantityString='" + quantityString + '\'' +
                ", image1='" + image1 + '\'' +
                ", image2='" + image2 + '\'' +
                ", fan_name='" + fan_name + '\'' +
                ", image3='" + image3 + '\'' +
                ", image4='" + image4 + '\'' +
                '}';
    }

    public String getFan_name() {
        return fan_name;
    }

    public void setFan_name(String fan_name) {
        this.fan_name = fan_name;
    }

    String image3;
    String image4;
    public String getQuantityString() {
        return quantityString;
    }

    public void setQuantityString(String quantityString) {
        this.quantityString = quantityString;
    }

    protected ProductListModel(Parcel in) {
        status = in.readInt();
        dataflow = in.readInt();
        product_id = in.readInt();
        price = in.readInt();
        quantity = in.readInt();
        product_name = in.readString();
        image = in.readString();
        description = in.readString();
        image1 = in.readString();
        image2 = in.readString();
        image3 = in.readString();
        image4 = in.readString();
        quantityString=in.readString();
        fan_name=in.readString();
    }

    public static final Creator<ProductListModel> CREATOR = new Creator<ProductListModel>() {
        @Override
        public ProductListModel createFromParcel(Parcel in) {
            return new ProductListModel(in);
        }

        @Override
        public ProductListModel[] newArray(int size) {
            return new ProductListModel[size];
        }
    };

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getImage3() {
        return image3;
    }

    public void setImage3(String image3) {
        this.image3 = image3;
    }

    public String getImage4() {
        return image4;
    }

    public void setImage4(String image4) {
        this.image4 = image4;
    }


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getDataflow() {
        return dataflow;
    }

    public void setDataflow(int dataflow) {
        this.dataflow = dataflow;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ProductListModel() {
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeInt(dataflow);
        dest.writeInt(product_id);
        dest.writeDouble(price);
        dest.writeInt(quantity);
        dest.writeString(product_name);
        dest.writeString(image);
        dest.writeString(description);
        dest.writeString(image1);
        dest.writeString(image2);
        dest.writeString(image3);
        dest.writeString(image4);
        dest.writeString(fan_name);
        dest.writeString(quantityString);
    }


}
