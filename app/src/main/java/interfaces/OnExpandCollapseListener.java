package interfaces;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;

public interface OnExpandCollapseListener {


    public void setOnExpand(String apiName, int position, ParentListItem parentListItem);
    public void setOnCollapse(String apiName, int position );
}
