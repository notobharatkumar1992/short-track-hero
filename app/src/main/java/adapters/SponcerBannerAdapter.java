package adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.SponcerBannerModel;
import model.SponcerModel;

public class SponcerBannerAdapter extends RecyclerView.Adapter<SponcerBannerAdapter.ViewHolder> {
    View v;
    private List<SponcerModel> sponcer;
    Context context;
    private LayoutInflater mInflater;
    private Map map = new HashMap<>();
    private Map immap = new HashMap<>();
    ArrayList<String> images = new ArrayList<>();
    int i = 0;
    private HashMap<Integer, ArrayList<SponcerModel>> arrayListHashMap = new HashMap<>();
    SponcerBannerModel model;

    ArrayList<SponcerBannerModel> arrayFavCategoModelList = new ArrayList<>();
    private SponcerBannerModel value=new SponcerBannerModel();

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.banner, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        value =arrayFavCategoModelList.get(position);
        AppDelegate.Log("getHASHMAP", value + "");
        for (int i = 0; i < value.getArrayListHashMap().size(); i++) {
            final int finalI = i;
            AppDelegate.getInstance(context).getImageLoader().get(value.getArrayListHashMap().get(i).getSponcer_img(), new ImageLoader.ImageListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    AppDelegate.LogE(error);
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        if (finalI % 6 == 0) {
                            holder.image1.setImageBitmap(response.getBitmap());
                        } else if (finalI % 6 == 1) {
                            holder.image2.setImageBitmap(response.getBitmap());
                        } else if (finalI % 6 == 2) {
                            holder.image3.setImageBitmap(response.getBitmap());
                        } else if (finalI % 6 == 3) {
                            holder.image4.setImageBitmap(response.getBitmap());
                        } else if (finalI % 6 == 4) {
                            holder.image5.setImageBitmap(response.getBitmap());
                        } else if (finalI % 6 == 5) {
                            holder.image6.setImageBitmap(response.getBitmap());
                        }
                    }
                }
            });
        }
    }
    public SponcerBannerAdapter(Context context, List<SponcerModel> sponcer) {
        this.context = context;
        this.sponcer = sponcer;
        fillChooserArray(sponcer);
    }
    private void fillChooserArray(List<SponcerModel> sponcer) {
        int position = -1;
        ArrayList<SponcerModel> models = new ArrayList<>();
        for (int i = 0; i < sponcer.size(); i++) {
            if (i % 6 == 0) {
                position++;
                models = new ArrayList<>();
            }
            models.add(sponcer.get(i));
            AppDelegate.Log("get", models + "");
            arrayListHashMap.put(position, models);
        }
        arrayFavCategoModelList.clear();
        for (int i = 0; i < arrayListHashMap.size(); i++) {
            arrayFavCategoModelList.add(new SponcerBannerModel(arrayListHashMap.get(i)));
            AppDelegate.LogT("arrayListHashMap pos = " + i + ", size  = " + arrayListHashMap.get(i).size());
        }
        AppDelegate.LogT("arrayFavCategoModelList = " + arrayFavCategoModelList.size() + ", arrayListHashMap = " + arrayListHashMap.size());
        // mHandler.sendEmptyMessage(2);
    }

    @Override
    public int getItemCount() {
        return arrayFavCategoModelList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image1, image2, image3, image4, image5, image6;

        public ViewHolder(View itemView) {
            super(itemView);
            image1 = (ImageView) itemView.findViewById(R.id.im1);
            image2 = (ImageView) itemView.findViewById(R.id.im2);
            image3 = (ImageView) itemView.findViewById(R.id.im3);
            image4 = (ImageView) itemView.findViewById(R.id.im4);
            image5 = (ImageView) itemView.findViewById(R.id.im5);
            image6 = (ImageView) itemView.findViewById(R.id.im6);
        }
    }
}