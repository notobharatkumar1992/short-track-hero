package adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.noto.shorttrackhero.R;

import java.util.ArrayList;

import model.CityListModel;
import model.SponcerCategoryModel;

public class CategoryAdapter extends ArrayAdapter<SponcerCategoryModel> {

    private final Activity context;
    ArrayList<SponcerCategoryModel> title;
    Integer image;

    public CategoryAdapter(Activity context, ArrayList<SponcerCategoryModel> title) {
        super(context, R.layout.dialog_listitem, title);
        this.context = context;
        this.title = title;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.dialog_listitem, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.listvalue);
        txtTitle.setText(title.get(position).getName());
        return rowView;
    }
}
