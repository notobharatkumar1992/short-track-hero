package adapters;


import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import fragments.DriverDetail;
import fragments.FanList;
import fragments.RaceDetail;
import fragments.RaceHistoryDetail;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import model.Driver;
import model.LoginModel;
import model.PostAysnc_Model;
import model.RaceHistroyModel;


public class RaceHistoryAdapter extends RecyclerView.Adapter<RaceHistoryAdapter.ViewHolder> {
    View v;
    private List<RaceHistroyModel> driver;
    FragmentActivity context;
    private LayoutInflater mInflater;
    private Map map = new HashMap<>();
    private Map immap = new HashMap<>();
    ArrayList<String> images = new ArrayList<>();
    private Bundle bundle;
    private LoginModel getProfile_model;
    private int user_id = 0;
    private int driver_id;
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.race_history_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        bundle = new Bundle();
        final RaceHistroyModel dv = driver.get(position);
        holder.competition_name.setText(dv.getCompetition_name() + "");
        holder.track_name.setText(dv.getRace_name() + "");

      /*  AppDelegate.getInstance(context).getImageLoader().get(dv.getDriver_img(), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppDelegate.LogE(error);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    holder.driver_img.setImageBitmap(response.getBitmap());
                }
            }
        });
*/
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle.putParcelable(Tags.parcel_racehistoryDetail, dv);
                AppDelegate.showFragment(context, new RaceHistoryDetail(), R.id.framelayout, bundle, null);
            }
        });
    }

    public RaceHistoryAdapter(FragmentActivity context, List<RaceHistroyModel> driver) {
        this.context = context;
        this.driver = driver;
    }

    @Override
    public int getItemCount() {
        return driver.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView driver_img;
        TextView competition_name, track_name;
        CardView cardView;

        public ViewHolder(View itemView) {
            super(itemView);
          /*  driver_img = (ImageView) itemView.findViewById(R.id.driver_img);*/
            competition_name = (TextView) itemView.findViewById(R.id.competition_name);
            track_name = (TextView) itemView.findViewById(R.id.track_name);
            cardView = (CardView) itemView.findViewById(R.id.card);
        }
    }
}