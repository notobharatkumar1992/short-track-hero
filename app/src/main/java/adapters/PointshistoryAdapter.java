package adapters;


import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Constants.Tags;
import fragments.CheckOut;
import fragments.DriverDetail;
import fragments.FanList;
import fragments.PointHistoryDetail;
import fragments.ShopHistoryDetail;
import model.Driver;
import model.PointsHistoryModel;

public class PointshistoryAdapter extends RecyclerView.Adapter<PointshistoryAdapter.ViewHolder> {
    View v;
    private List<PointsHistoryModel> pointsHistory;
    FragmentActivity context;
    private LayoutInflater mInflater;
    private Map map = new HashMap<>();
    private Map immap = new HashMap<>();
    ArrayList<String> images = new ArrayList<>();
    private Bundle bundle;
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.points_history_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
         bundle=new Bundle();
        final PointsHistoryModel dv = pointsHistory.get(position);
        holder.driver_name.setText(dv.getDriver_name() + "");
        holder.competition_name.setText(dv.getCompetitions_name() + "");
        holder.race_name.setText(dv.getRace_name() + "");
        holder.rank.setText(dv.getCurrent_rank() + "");
        holder.total_point.setText(dv.getTotal_point() + "");
        holder.total_credit.setText(dv.getTotal_credit() + "");
        String sdate = null,stime=null;
        try {
            sdate = new SimpleDateFormat("dd MMM yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(dv.getDate()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.date.setText(sdate + "");
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putParcelable(Tags.parcel_pointsHistoryItem,dv);
                AppDelegate.showFragment(context, new PointHistoryDetail(), R.id.framelayout, bundle, null);
            }
        });
    }
    public PointshistoryAdapter(FragmentActivity context, List<PointsHistoryModel> pointsHistory) {
        this.context = context;
        this.pointsHistory = pointsHistory;
    }
    @Override
    public int getItemCount() {
        return pointsHistory.size();
    }
    class ViewHolder extends RecyclerView.ViewHolder {
CardView cardView;
        TextView driver_name, competition_name,race_name,total_point,total_credit, date, rank;
        public ViewHolder(View itemView) {
            super(itemView);
            driver_name = (TextView) itemView.findViewById(R.id.driver);
            competition_name = (TextView) itemView.findViewById(R.id.competion_name);
            race_name = (TextView) itemView.findViewById(R.id.race_name);
            total_point = (TextView) itemView.findViewById(R.id.points);
            rank = (TextView) itemView.findViewById(R.id.rank);
            total_credit = (TextView) itemView.findViewById(R.id.credit);
            date = (TextView) itemView.findViewById(R.id.date);
            cardView=(CardView)itemView.findViewById(R.id.card);
        }
    }
}