package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.R;

import java.util.ArrayList;

import model.ProductListModel;
import model.SponcerModel;

/**
 * Created by admin on 04-07-2016.
 */
public class SponcerAdapter extends BaseAdapter {
    ArrayList<SponcerModel> productListModels;
Context context;

    public SponcerAdapter(Context context, ArrayList<SponcerModel> productListModels) {
        this.productListModels = productListModels;
        this.context = context;
    }

    @Override
    public int getCount() {
        return productListModels.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView= inflater.inflate(R.layout.sponcer_item, null, true);
        final ImageView shopImage=(ImageView)rowView.findViewById(R.id.shopimg);
        AppDelegate.getInstance(context).getImageLoader().get(productListModels.get(position).getSponcer_img(), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppDelegate.LogE(error);
            }
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null)
                    shopImage.setImageBitmap(response.getBitmap());
            }
        });
        return rowView;
    }
}
