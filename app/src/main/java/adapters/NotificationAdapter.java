package adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.R;

import java.util.ArrayList;

import model.CityListModel;
import model.NotificationModel;

public class NotificationAdapter extends ArrayAdapter<NotificationModel> {

    private final Activity context;
    ArrayList<NotificationModel> title;
    Integer image;

    public NotificationAdapter(Activity context, ArrayList<NotificationModel> title) {
        super(context, R.layout.dialog_item, title);
        this.context = context;
        this.title = title;
        AppDelegate.LogT("NotificationAdapter called");
    }

    @Override
    public int getCount() {
        return title.size();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.dialog_item, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.listvaluetitle);
        TextView message = (TextView) rowView.findViewById(R.id.listvalue);
        txtTitle.setText(title.get(position).getType());
        AppDelegate.Log("title", title.get(position).getType() + "");
        message.setText(title.get(position).getMessage());
        return rowView;
    }
}
