package adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.noto.shorttrackhero.R;


public class NavigationAdapter extends ArrayAdapter<String> {
    // TODO Auto-generated constructor stub
    private final Activity context;
    private final String[] title;
    Integer[] icon;


    public NavigationAdapter(Activity context, String[] title, Integer[] icon) {
        super(context, R.layout.listitem, title);
        {
            this.context = context;
            this.title = title;
            this.icon = icon;
        }
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.listitem, parent, false);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.title);
        ImageView icons = (ImageView) rowView.findViewById(R.id.icon);
        txtTitle.setText(title[position]);
        icons.setImageResource(icon[position]);


        return rowView;
    }
}
