package ExpendibleRecyclerView;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.bignerdranch.expandablerecyclerview.Model.ParentWrapper;
import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.R;
import fragments.RaceDetail;
//import com.bignerdranch.expandablerecyclerview.Model.ParentObject;

import java.util.ArrayList;
import java.util.List;

import Constants.Tags;
import interfaces.OnExpandCollapseListener;


public class Adapter extends ExpandableRecyclerAdapter<TitleViewHolder, SubTitleViewHolder> {
    private LayoutInflater mInflater;
    List<ParentListItem> parentItemList;
    static StringBuilder items = new StringBuilder();
    int parent_position, position;
    private TitleViewHolder parentViewHolder;

    FragmentActivity context;

    ArrayList<Data> structures;

    OnExpandCollapseListener onExpandCollapseListener;
    private ParentListItem result;
    Bundle bundle=new Bundle();
    private Data data;
    private View view;

    public Adapter(FragmentActivity context, ArrayList<ParentListItem> parentItemList, ArrayList<Data> structures, OnExpandCollapseListener onExpandCollapseListener) {
        super(parentItemList);
        this.context = context;
        mInflater = LayoutInflater.from(context);
        this.parentItemList = parentItemList;
        this.structures = structures;
        this.onExpandCollapseListener = onExpandCollapseListener;
    }

    @Override
    public TitleViewHolder onCreateParentViewHolder(ViewGroup viewGroup) {
         view = mInflater.inflate(R.layout.parent, viewGroup, false);
        return new TitleViewHolder(view);
    }

    @Override
    public SubTitleViewHolder onCreateChildViewHolder(ViewGroup viewGroup) {
        View view = mInflater.inflate(R.layout.child, viewGroup, false);
        return new SubTitleViewHolder(view);
    }

    @Override
    public void onBindParentViewHolder(final TitleViewHolder parentViewHolder, final int position, ParentListItem parentListItem) {
        parent_position = position;
        this.parentViewHolder = parentViewHolder;
         data = (Data) parentListItem;
        parentViewHolder.title.setText(data.getTitle());
       if(data.isExpend==1)
       {
           if(data.getChildItemList().size()==0){
               parentViewHolder.add.setVisibility(View.INVISIBLE);
               parentViewHolder.add.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.plus));
           }else {
               parentViewHolder.add.setVisibility(View.VISIBLE);
               parentViewHolder.add.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.minus));
           }
       }
        else{
           parentViewHolder.add.setVisibility(View.VISIBLE);
           parentViewHolder.add.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.plus));
       }

    }
    public void onBindChildViewHolder(SubTitleViewHolder subTitleViewHolder, final int i, Object object) {
        final ChildGetterSetter crimeChild = (ChildGetterSetter) object;
        subTitleViewHolder.subtitle.setText(crimeChild.getSubtitle());
        subTitleViewHolder.subtitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  bundle.putString("Parent", data.getTitle());
                bundle.putInt("interested",2);
                bundle.putParcelable(Tags.parcel_raceDetail, crimeChild);
                AppDelegate.showFragment(context, new RaceDetail(), R.id.framelayout, bundle, null);
            }
        });
    }
    @Override
    public void onParentListItemExpanded(int position) {
       // super.onParentListItemExpanded(position);
        AppDelegate.LogT("onParentListItemExpanded position = " + position);
        if(position<0){

        }else {
            Object parent = mItemList.get(position);
            collapseAllParents();    // Alternatively keep track of the single item that is expanded and explicitly collapse that row (more efficient)
            expandParent(((ParentWrapper) parent).getParentListItem());
            AppDelegate.Log("expand parent", ((ParentWrapper) parent).getParentListItem() + "");
            data = (Data) ((ParentWrapper) parent).getParentListItem();
            bundle.putString("Parent", data.getTitle());
            onExpandCollapseListener.setOnExpand("Adapter", position, ((ParentWrapper) parent).getParentListItem());
        }
        // parentViewHolder.add.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.minus));
    }
    @Override
    public void onParentListItemCollapsed(int position) {
        if (position < 0) {

        } else {
            Object parent = mItemList.get(position);
            TitleViewHolder titleViewHolder = new TitleViewHolder(view);
            // parentViewHolder.add.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.plus));
        }
    }
}