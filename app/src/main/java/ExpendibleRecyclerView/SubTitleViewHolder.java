package ExpendibleRecyclerView;

import android.view.View;
import android.widget.TextView;


import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;
import com.noto.shorttrackhero.R;


public class SubTitleViewHolder extends ChildViewHolder {
    TextView subtitle, delete;

    public SubTitleViewHolder(View itemView) {
        super(itemView);
        subtitle = (TextView) itemView.findViewById(R.id.size);

    }
}
